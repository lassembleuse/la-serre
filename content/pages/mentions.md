---
date: 2019-08-01T13:17:31.000+00:00
title: Mentions légales
menu:
  footer:
    weight: 2

---
## Editeur

France Active Lorraine
6, boulevard du 21ème Régiment d’Aviation
54000 Nancy

Directrice de la publication : Claire Claudel, Directrice

## Hébergement

Site hébergé chez [SOS FUTUR](https://www.sos-futur.fr/)

## Crédits

* Design, logo et illustrations : [Studio Indigo](http://studio-indigo.eu/), **tous droits réservés**
* Développement du site : [L'assembleuse](https://www.lassembleuse.fr/)

## Réutilisation des contenus

Sauf mention contraire, les contenus de ce site sont sous licence [Creative Commons BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

## Comment créditer les contenus de ce site

L’utilisateur qui souhaite réutiliser les contenus de ce site doit impérativement indiquer :

1. Le titre associé au contenu,
2. La paternité du contenu par la mention suivante : « Source : la Serre à projets– www.laserre.org »,
3. Le suivi de la date à laquelle le contenu a été extrait du site : « JJ/MM/AAAA »,
4. L’indication de la licence sous laquelle le contenu a été mis à disposition