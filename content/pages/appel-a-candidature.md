---
date: 2019-08-01T13:17:31.000+00:00
title: Appel à candidatures
main_picture: "/uploads/illu-appel-candidature.svg"
short_description: |-
  L'appel à candidatures de la Serre à projets **est clos** depuis le **19 février 2023**.

  Rendez-vous dès **fin 2023** pour un nouvel appel à candidatures !

  En attendant, vous avez toujours la possibilité de télécharger le cahier des charges de l'appel à candidatures pour vous y préparer dès à présent !

  > **Vous avez des questions ?** [**Contactez-nous !**](https://www.laserre.org/contact/)
curvy_bg: true
layout: appel
appel_candidature_doc: "/uploads/cahier-des-charges-appel-a-porteurs-de-projets-v4.pdf"
menu:
  main:
    weight: 5

---
