---
date: 
title: Quartiers en transition
main_picture: ''
short_description: |-
  ![](/uploads/illustration-page-quartiers-en-transition-test-3.png)

  ### **La Serre à projets a lancé en 2021 les Quartiers en Transitions !**

  La Serre déploie son dispositif plus spécifiquement sur les quartiers prioritaires de la politique de la Ville (QPV) de la Métropole du Grand Nancy. L’objectif est d’accompagner la création d’activités répondant aux besoins spécifiques des quartiers en lien avec la transition écologique et l’Économie Sociale et Solidaire (ESS).

  Notre ambition : « _Un projet de Quartier POUR le Quartier et AVEC le Quartier !_ »

  **Le fonctionnement et les étapes du dispositif**

  Tous les ans, 2 QPV sont sélectionnés et disposent d’un accompagnement spécifique mis en place en collaboration avec la collectivité concernée. En 2021, les 2 QPV sélectionnés étaient les Provinces (Laxou) et les Nations (Vandoeuvre).

  En 2022, la Californie (Jarville) et un site du Plateau de Haye (Laxou, Maxéville et Nancy) ont été sélectionnés.

  Après leur sélection, la première étape consiste à rencontrer la collectivité avec les différents services et élus intervenants dans le champ d’actions de la Serre à Projets. Cette première réunion permet de faire un premier état des lieux des dynamiques existantes (thématiques abordées et acteurs locaux).

  Puis des rencontres sont réalisées avec les acteurs locaux (associations des quartiers, conseil citoyens, etc.). En fonction du contexte sanitaire, un atelier participatif associant la population est également mis en place. Cette étape permet d’effectuer la remontée des besoins des habitants et d’identifier les thématiques à cibler (Alimentation, cadre de vie, économie circulaire, lien social, etc.). De ce travail, en découle 4 propositions d’idées de projets par QPV. Parmi ceux-ci, le Comité de Pilotage de la Serre en choisisse 2 qui feront l’objet d’une étude d’opportunité qui sera également validée par ce comité.

  Ensuite, comme pour le dispositif général de la Serre ([https://www.laserre.org/presentation/](https://www.laserre.org/presentation/ "https://www.laserre.org/presentation/")), un appel à candidatures est réalisé et un jury sélectionne les porteurs projets. Les lauréats suivent ensuite l’accompagnement de la Serre durant plusieurs mois jusqu’au lancement de l’activité avec la réalisation d’une étude de faisabilité à réaliser.

  **2021/2022 : Les projets pour « Les Provinces » à Laxou et « Les Nations » à Vandœuvre.**

  * **Pour « Les Provinces » - Laxou :**

  **Les deux études d’opportunité réalisées portaient sur** [**Un tiers-lieu de la transition écologique**](https://www.laserre.org/projets/2022/le-tiers-lieu-de-la-transition-quartiers-en-transition-laxou/) **et Une recyclerie jouets et enfance.**

  **C’est le projet de Tiers-lieu** proposé par [**l’Association _Si l’on se parlait_ qui a été retenu**](https://www.laserre.org/projets/2022/le-tiers-lieu-de-la-transition-quartiers-en-transition-laxou/ "Tiers lieu")

  * **Pour « Les Nations » - Vandœuvre :**

  **Les deux études d’opportunité réalisées portaient sur Une cantine solidaire et Une cuisine partagée**

  C’est le projet de [**restaurant et traiteur solidaire**](https://www.laserre.org/projets/2022/la-cantine-solidaire-quartiers-en-transition-et-serre-a-projets-generaliste/ "Restaurant et traiteur solidaire") proposé par l’Association Khamsa qui a été retenu.

  > Vous avez des questions ?
  >
  > **N'hésitez pas, contactez-nous au 07 66 43 14 48 ou contact@laserre.org**
curvy_bg: false
layout: ''
texte: |-
  ### La Serre à projets lance les Quartiers en Transitions !

  La Serre déploie son dispositif plus spécifiquement sur les quartiers prioritaires de la politique de la Ville (QPV) de la Métropole du Grand Nancy. L’objectif est d’accompagner la création d’activités répondant aux besoins spécifiques de deux quartiers sélectionnés : **« Les Provinces » à Laxou et « Les Nations » à Vandœuvre.**

  Vous souhaitez créer une activité en lien avec la transition écologique et solidaire dans ces quartiers ? Les thématiques suivantes vous intéressent ? Consultez les études et répondez à l'[**appel à candidatures qui est ouvert du 5 janvier 2022 au 9 février 2022**]().

  Voici les thématiques qui ont été sélectionnées :

  #### \[Les Nations - Vandœuvre\] :

  * **Une activité de cuisine partagée**

  #### \[Les Provinces - Laxou\] :

  * **Une recyclerie "Jouets et enfance"**
  * **Un tiers-lieu de la transition écologique**

  À noter que dans ces quartiers, il sera également possible de développer le projet de **cantine solidaire**.

  Retrouvez l'ensemble des projets de la promotion 2022 [**ici**](https://www.laserre.org/projets/2022/ "Promo 2022") !

  > Vous avez des questions ? Besoin d'aide pour compléter un dossier de candidatures ? **N'hésitez pas, contactez-nous au 07 67 48 83 66 ou contact@laserre.org**
menu:
  main:
    weight: 6

---
