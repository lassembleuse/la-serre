---
date: 2019-08-01T13:17:31.000+00:00
title: Présentation
layout: presentation
page_title: Qu’est-ce que la Serre à projets ?
why_title: Pourquoi une Serre à projets ?
why_content: |-
  La transition écologique est un enjeu majeur pour nous tous ! Ensemble, il nous faut œuvrer à un changement très profond de nos modes de vie vers la sobriété et la durabilité. Pour cela, il est essentiel de faire preuve d’imagination pour créer et développer de nouvelles activités qui permettent au territoire d’opérer sa transition. C’est ce que propose la Serre à projets !

  Ce dispositif, animé par France Active Lorraine et la SCIC Kèpos, vise à repérer des besoins non satisfaits sur le territoire, à imaginer des solutions pour y répondre, à étudier l’opportunité et la faisabilité des projets qui en sont issus, et à les transmettre à des porteurs de projets de l’Économie Sociale et Solidaire.

  Il s’agit donc bien d’une méthodologie d’entrepreneuriat inversé, où l’on part des besoins du territoire, et non pas des porteurs de projets ! Particularité de cet outil de développement territorial : il est thématisé sur la transition écologique !
why_picture: "/uploads/ideogramme-blanc.png"
how_title: Comment cela se passe-t-il ?
how_content: L’action de la Serre à projets se déroule en 5 étapes, renouvelées chaque
  année.
how_steps:
- content: Détecter des besoins sociaux et des opportunités socio-économiques via
    un réseau de capteurs d’idées.
  icon: "/uploads/illu-etape-1.png"
- content: Inventer des réponses collectivement et valider leur pertinence et leur
    viabilité grâce à une étude d’opportunité.
  icon: "/uploads/illu-etape-2.png"
- content: Transmettre le projet à un entrepreneur qualifié ou à une entreprise sociale
    existante via un appel à candidatures.
  icon: "/uploads/illu-etape-3.png"
- content: Accompagner les porteurs de projets jusqu’à la création de l’entreprise
    sociale, après validation des projets grâce à une étude de faisabilité.
  icon: "/uploads/illu-etape-4.png"
- content: Lancer la nouvelle activité.
  icon: "/uploads/illu-etape-5.png"
how_action: Que vous représentiez une entreprise, une association, un collectif de
  citoyens ou que vous agissiez à titre individuel, vous pouvez participer au dispositif
  en faisant remonter vos idées, ou en répondant à l’appel à candidatures !
who_choose_title: Qui choisit les projets ?
who_choose_content_1: 'Les projets qui sont étudiés dans le cadre de la Serre à projets
  ont été sélectionné par un comité de pilotage qui associe les partenaires opérationnels
  et financiers du dispositif. Ceux-ci sont en prise directe avec le territoire et
  ses acteurs. Son rôle est le suivant :'
who_choose_roles:
- Sélectionner les idées à faire émerger.
- Suivre les études d’opportunité et lancer l’appel à candidatures.
- Choisir les porteurs de projets.
- Valider les études de faisabilité.
- Soutenir la création de l’activité.
who_choose_roles_picture: "/uploads/illu-comite-de-pilotage.png"
who_choose_content_2: Les projets supportés font donc l’objet d’un accompagnement
  collectif par l’ensemble des parties prenantes du dispositif.
territory_title: Sur quel territoire ?
territory_content: 'La Serre à projets est active sur le bassin de vie de Nancy :
  la Métropole du Grand Nancy au premier chef, et par extension le sud du département
  de Meurthe-et-Moselle, incluant Toul, Pont-à-Mousson et Lunéville.'
territory_picture: "/uploads/illu-carte.png"
for_who_title: Pour qui ?
for_who_content: La Serre à projets étude l’opportunité et la faisabilité de projets,
  qu’elle met à la disposition des forces vives du territoire.
for_who_pictures:
- title: Entreprises sociales
  picture: "/uploads/illu-entreprises-sociales.png"
- title: Associations, Collectifs de citoyen
  picture: "/uploads/illu-assos-collectifs.png"
- title: Porteurs individuels
  picture: "/uploads/illu-individuel.png"
menu:
  main:
    weight: 3

---
