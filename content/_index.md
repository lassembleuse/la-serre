---
title: Page d'accueil
date: 2020-01-08T21:35:58.000+01:00
main_picture: "/uploads/illustration-accroche.svg"
baseline: Initier de nouvelles activités engagées dans la transition écologique
short_description: La Serre à projets a pour mission de faire émerger sur la région
  nancéienne de nouvelles activités en lien avec la transition écologique. Pour cela,
  elle repère sur le territoire des besoins non satisfaits, elle imagine des solutions
  pour y répondre, étudie l’opportunité et la faisabilité des projets qui y sont liés,
  avant de les transmettre à des porteurs de projets de l’Économie Sociale et Solidaire
  (ESS). Véritable Fabrique à projets d’Utilité Sociale thématisée sur la transition
  écologique, elle veut concourir à l’épanouissement d’un entrepreneuriat durable
  et responsable sur le sud de la Meurthe-et-Moselle.
sitemap:
  priority: 0.9
menu:
  main:
    name: Accueil
    weight: 1

---
