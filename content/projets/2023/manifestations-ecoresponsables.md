---
weight: 6
title: Manifestations écoresponsables
picture: "/uploads/picto-ecomanif.png"
category: ''
description: "Déploiement d’une activité dans le but d’accompagner les acteurs de
  l’événementiel de la Meurthe-et-Moselle souhaitant initier ou approfondir leur démarche\nécoresponsable. "
etude_opportunite: ''
nom_orga_porteur: Victor TRAPP et Eco-Manifestations Alsace
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
Face aux multiples défis environnementaux et sociétaux auxquels sont confrontées nos sociétés, les acteurs de l’événementiel sont également concernés par la transition écologique dans leurs activités. L’intérêt pour ce sujet provient également du public qui est réceptif et ouvert à de nouvelles expérimentations telles que le recours à la consigne pour pouvoir utiliser de la vaisselle réutilisable, le choix d'une alimentation plus végétale, l'utilisation desmobilités douces … En ce sens, les événements sont des terrains d’actions essentiels pour mettre en œuvre le monde de demain.

Cette nouvelle structure intégrera le réseau Eco-Manifestations Réseau Grand Est (EMeRGE) qui rassemble des associations fédérant leurs compétences en transition écologique pour promouvoir les comportements écoresponsables auprès des organisateurs de manifestation de la région Grand Est.