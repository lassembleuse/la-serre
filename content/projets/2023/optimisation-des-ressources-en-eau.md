---
weight: 1
title: Optimisation des ressources en eau
picture: "/uploads/picto-eau.png"
category: ''
description: Le changement climatique va transformer la question de l’accès à l’eau
  en un enjeu critique. Dès lors, il devient essentiel d’optimiser l’utilisation de
  cette ressource, en limitant de façon très ambitieuse son gaspillage. Or, la manière
  dont est utilisée l’eau dans une habitation ou un bâtiment tertiaire est très largement
  perfectible. On pourrait ainsi imaginer l’émergence sur le territoire d’un installateur
  spécialisé dans l’optimisation des ressources en eau.
etude_opportunite: "/uploads/laserre_etude_opportunite_optimisation-des-ressources-en-eau.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''
draft: true

---
