---
weight: 7
title: Pôle d'excellence du cuir
picture: "/uploads/picto-cuir-a-detourrer.png"
category: ''
description: Création d’un lieu dédié aux métiers du cuir permettant une mutualisation
  de moyens entre professionnels, la réalisation de formations et la mise en place
  d’une boutique et d’un showroom.
etude_opportunite: ''
nom_orga_porteur: SARL MAUDE IN France / L’URSIDÉ / ANTHELIA / ALERION
nom_contact_porteur: Maude VANOBBERGHEN, Julien CHALON, Audrey GASC et Quentin SIMARD
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
Une grande partie des entreprises exerçant une activité artisanale en relation avec le cuir (sellerie, maroquinerie, tapisserie, …), font face à de nombreuses problématiques freinant leur intégration réelle au tissu économique, social et solidaire local, départemental voire régional.

Ce lieu aura vocation à devenir un lieu :

* de vie et de culture avec des animations régulières,
* de référence pour les clients, en permettant une visibilité de l’ensemble de métiers du cuir présents localement ;
* de collaboration accrue et de travail coordonné.

L’ouverture de ce lieu est envisagé à l’horizon 2025, sous la forme d’un site d’activités «multipolaire» répondant aux attentes et aux besoins des artisans.