---
weight: 4
title: Garage Retrofit
picture: "/uploads/picto-retrofit.png"
category: ''
description: Le rétrofit est une opération consistant à convertir une voiture thermique
  à l’électrique. Il remplace le moteur essence ou diesel par un moteur électrique.
  Autorisé en France depuis 2020, le rétrofit permet de donner une seconde vie à sa
  voiture. En permettant de convertir un véhicule à motorisation thermique en motorisation
  électrique, le rétrofit permet d’accélérer le passage à l’électrique, d’augmenter
  le nombre de véhicules électriques en circulation sur le territoire et de diminuer
  la pollution émises par les transports.
etude_opportunite: "/uploads/laserre_etude_opportunite_garage-retrofit.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
