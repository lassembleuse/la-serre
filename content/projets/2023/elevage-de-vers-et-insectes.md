---
weight: 3
title: Elevage de vers et insectes
picture: "/uploads/picto-insectes.png"
category: ''
description: |+
  Selon la FAO, l’organisation des nations unies pour  l’alimentation et l’agriculture, la demande de viande va quasiment doubler d’ici 2050. Les ressources de notre planète ne suffiront pas à satisfaire cette demande. La culture d’insectes nécessite 10 à 100 fois moins de ressources et 100 grammes de protéines de vers correspondent à trois steaks hachés.

  Les finalités sont diverses. L’élevage d’insectes peut être une option de diversification alimentaire chez les humains et le bétail. Aussi, il est possible d’envisager une culture de vers pour permettre la production de soie par exemple.

etude_opportunite: "/uploads/laserre_etude_opportunite_elevage-de-vers-et-insectes.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''
draft: true

---
