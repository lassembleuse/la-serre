---
weight: 3
title: Recyclerie de sport, loisirs et jouets
picture: "/uploads/picto-sport.png"
category: ''
description: |-
  Pour réduire les déchets émanant de cette consommation de biens, l’ADEME préconise le réemploi, la réparation, le recyclage. En ce sens, la recyclerie de sport, loisirs et jouets permettrait de collecter des équipements sportifs et des jouets dont certains n’ont plus d’usage. Ainsi, au lieu de devenir des déchets, ils sont contrôlés, nettoyés, réparés, et vendus à des prix faibles afin de leur donner une seconde vie.

  Ce serait donc une véritable structure emblématique du réemploi et de l’économie circulaire et solidaire, qui répondra aux enjeux environnementaux actuels, mais aussi à des besoins très concrets d’équipement pour les sportifs locaux et familles, à moindre coût.
etude_opportunite: "/uploads/laserre_etude_opportunite_recyclerie-du-sport-et-loisirs.pdf"
nom_orga_porteur: LORRAINE SPORT SERVICES et ENVIE 2E // Alexandra PIANESE
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
2 porteurs de projet se sont positionnés sur la thématique, dont :

**UNE RECYCLERIE SPPORTIVE**, portée par la SCIC Lorraine Sport Services et la SCIC Envie 2E.

Le projet vise à accompagner les sportifs amateurs, les clubs, les licenciés et les collectivités dans l'acquisition de matériel sportif de seconde main, promouvoir la notion de développement durable du matériel sportif, être acteur du changement du comportement de consommation et de favoriser l'insertion professionnelle avec la création de nouveaux emplois.

**UNE RECYCLERIE DE JOUETS**, portée par Alexandra PIANESE.

Création d’une structure d’insertion permettant le réemploi de jouets et livres sur le bassin mussipontain. La porteuse souhaite proposer de la vente en ligne et en boutique, pour les particuliers et les professionnels de la petite enfance. Le projet s’articulerait autour de plusieurs activités : collecte, tri, recyclage (des jouets cassés), recomposition, nettoyage, contrôle qualité et vente.