---
weight: 6
title: Valorisation des coproduits et des déchets brassicoles et de boulangeries
picture: "/uploads/picto-biere-et-pain.png"
category: ''
description: |-
  D’après une étude de l’Ademe datant de 20162 sur le gaspillage alimentaire, les boulangers constateraient une perte de pain de près de 9 % par rapport à leur production totale. Sur une année, ce pourcentage d’invendus représenterait 150 000 tonnes.

  Le secteur brassicole génère également des déchets et des coproduits liés à la fabrication de bières. En effet, quasiment chaque étape de la production engendre des déchets et co-produits différents.

  L’implantation d’une activité de valorisation des coproduits et déchets du secteur brassicole et des boulangeries aurait pour mission d’accompagner les brasseries et les boulangeries à réduire leur impact environnemental et à imaginer des solutions pour revaloriser les coproduits et déchets issus de la fabrication de la bière et du pain, ainsi que de leurs invendus.
etude_opportunite: "/uploads/laserre_etude_opportunite_valorisation-des-coproduits-du-secteur-brassicole.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''
draft: true

---
