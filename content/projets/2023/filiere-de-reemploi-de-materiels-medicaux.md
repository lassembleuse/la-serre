---
weight: 5
title: Filière de réemploi de matériels médicaux
picture: "/uploads/picto-medical.png"
category: ''
description: |-
  Création d’une communauté de valorisation des appareils médicaux sous forme de collectif constitué des différentes parties prenantes du secteur de la santé sur le territoire et dont l’objectif est de coordonner une ﬁlière de reconditionnement des appareils médicaux. Cette
  organisation vise ainsi à proposer des solutions économiques et écologiques aﬁn de développer l’accès aux soins.
etude_opportunite: ''
nom_orga_porteur: CARDIOLAB
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
Créée en 2019, par deux ingénieurs biomédicaux, Cardiolab SAS est une TPE spécialisée dans l’ingénierie biomédicale, en particulier le reconditionnement des appareils médicaux (notamment en cardiologie).

Ce projet coopératif de valorisation des appareils médicaux vise à répondre aux enjeux identifiés par le ministère de la santé, les ARS, les praticiens et les instances d’exercice coordonné de la médecine. 

Ces enjeux prioritaires sont de trois ordres :

1. problématique de l’accès aux soins ;
2. problématique du bilan environnemental du système de soins ; 
3. nécessité de déployer un système de soin plus résilient.