---
weight: 1
title: Valorisation ou lutte contre des espèces invasives
picture: "/uploads/picto-eee.png"
category: ''
description: Les introductions, volontaires ou non, d’espèces exotiques animales et
  végétales, prennent des proportions désastreuses. Une espèce exotique peut devenir
  envahissante quand elle s’installe ou se développe dans un écosystème en devenant
  une perturbation nuisible à la biodiversité du milieu qui l’accueille. Valoriser
  une espèce invasive d’un point de vue socio-économique constituerait un levier intéressant
  dans la régulation de sa population.
etude_opportunite: "/uploads/laserre_etude_opportunite_valorisation-ou-lutte-des-especes-invasives.pdf"
nom_orga_porteur: PROFILIA // Maude BAYLE
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
2 porteurs de projets se sont positionnés sur cette thématique :

La société **PROFILIA** avec son projet **RenouFILIA** qui a pour objectif la régulation et à long terme l’élimination de la renouée du Japon sur les territoires lorrains via la valorisation chimique et la valorisation énergétique. Le principal objectif de ce projet est le renforcement structurel de la filière de valorisation des espèces invasives.

**Maude BAYLE** qui porte le projet **LA RENOUÉE** qui consiste à réussir à transformer la renouée du Japon non plus par la vannerie (qui est un type de valorisation déjà connu) mais grâce à d’autres procédés, pour qu’elle devienne une alternative aux panneaux de bois aggloméré utilisé dans le domaine de l’agencement menuisé.