---
weight: 2
title: Développement ou renforcement de l'offre en protection et couches lavables
picture: "/uploads/picto-couches.png"
category: ''
description: |-
  La solution des couches lavables existe sur notre territoire, mais est délaissée par certains usagers du fait du manque de connaissances sur le sujet, ou une perception comme « trop chère » ou « trop compliquée à mettre en place ».
  L’idée ici est donc de muscler les activités existantes à ce sujet et/ou d'en développer d'autres, notamment sur les protections lavables.
etude_opportunite: "/uploads/laserre_etude_opportunite_developpement-ou-renforcement-de-l-offre-en-protection-et-couches-lavables.pdf"
nom_orga_porteur: SUPER KWETSCH // COCOTTE DE MAILLES
nom_contact_porteur: Hélène CONNES (SUPER KWETSCH) // Sandra MARTINEZ (COCOTTE DE
  MAILLES)
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
2 porteuses de projet se sont positionnées sur cette thématique :

**SUPER KWETSCH** est une culotte de règles bio, locale et solidaire, made in Nancy. La porteuse souhaite que son projet respecte 3 valeurs essentielles :  une dimension bio, locale et solidaire.

**COCOTTE DE MAILLES** est spécialisée dans laconception de vêtements pour bébés dès la naissance exclusivement en laine Mérinos française et en jersey de lin européen. Une collection évolutive et adaptée aux couches lavables ainsi qu'une culotte de protection 100% laine Mérinos française. Elle a pour objectif de faciliter la motricité et le confort du bébé en couche lavable à partir de fibres nobles et écologiques.