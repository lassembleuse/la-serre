---
title: La cantine solidaire
picture: "/uploads/Picto-Cantine-Solidaire.png"
category: Agriculture, alimentation
description: L'objectif est d'ouvrir à Lunéville un restaurant d'un genre nouveau,
  qui restaure non seulement le corps mais également l'esprit et les liens de solidarité
  entre les citoyens. Y seront proposés des repas sains, goûtus et roboratifs, à base
  de produits vraiment locaux, frais et de saison, issus de l'agriculture biologique
  et cuisinés par des employés rémunéré.
etude_opportunite: ''
photo: ''
weight: 2
nom_orga_porteur: L'Écurie n°8
nom_contact_porteur: Adrien Laroque
tel_porteur: 06 73 85 80 50
mail_porteur: contact@ecurie8.org
adresse_porteur: ''
site_web_porteur: http://ecurie8.org/
ville_porteur: Lunéville
draft: true

---
L'objectif est d'ouvrir à Lunéville un restaurant d'un genre nouveau, qui restaure non seulement le corps mais également l'esprit et les liens de solidarité entre les citoyens. Y seront proposés des repas sains, goûtus et roboratifs, à base de produits vraiment locaux, frais et de saison, issus de l'agriculture biologique et cuisinés par des employés rémunérés.

Le lieu sera attentif au respect des sensibilités et des interdits ou  intolérances alimentaires tout en ouvrant les horizons sur d'autres cultures. La disparité des moyens de subsistance sera prise en compte par l'application de tarifs différenciés. De plus, possibilité sera donnée au consommateur d'offrir un repas ou une boisson de manière anonyme (système inspiré du caffè sospeso des maisons du peuple italiennes).

Ce projet sera construit avec d'autres acteurs locaux, tous bords et tous secteurs confondus, pour un ancrage territorial aussi pertinent que possible.