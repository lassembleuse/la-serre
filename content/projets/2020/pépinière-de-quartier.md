---
weight: 7
title: La pépinière de quartier
picture: "/uploads/Picto Pepiniere de quartier.png"
category: ''
description: "La Causerie sera une pépiniere de quartier ouverte à tous et idéalement située dans le quartier Rives de Meurthe. L'association a  pour objectif de créer du lien social en ouvrant un lieu aux citoyens autour du thème de la re-végétalisation urbaine responsable et locale de Nancy."
etude_opportunite: ''
nom_orga_porteur: La causerie
nom_contact_porteur: Emilie Clavel
tel_porteur: 06 71 26 16 75
mail_porteur: lacauserie54@gmail.com
adresse_porteur: ''
ville_porteur: Nancy
site_web_porteur: 
photo: ''

---
La Causerie sera une pépiniere de quartier ouverte à tous et idéalement située dans le quartier Rives de Meurthe. L'association a  pour objectif de créer du lien social en ouvrant un lieu aux citoyens autour du thème de la re-végétalisation urbaine responsable et locale de Nancy. Ce sera un lieu d’échange, de partage à l’échelle d’un quartier pour créer du lien, de la résilience dans la bonne humeur. Elle promouvra les synergies entre activités professionnelles et citoyennes, s’inscrira dans une perspective de relocalisation de la production, participera à la vie du quartier et à la protection de l’environnement dans un esprit coopératif inter-générationel et participatif.

Nous y trouverons : 
- Une serre non chauffée pour y faire grandir une petite production permacole et locale  d'aromatiques, fleurs et plantes annuelles, plants de légumes, et arbustes.
- Un composteur partagé et un poulailler pour traiter les biodéchets des adhérents.
- Un accès libre à un espace de fleurs à couper et de petits fruitiers.

Toute une série d'actions de sensibilisation à l'environnement, à la réduction des déchets, et au jardinage naturel sera proposée :
- Des ateliers « nature » pour adultes et enfants sur différents thèmes en fonction des saisons (fabrication de bombes à graines, nichoirs, hôtel à insectes, semis, récoltes des graines, herbiers, peintures naturelles...) et des ateliers réduction des déchets (fabrication d'un composteur, de produits ménagers, bocaux lacto-fermentés...)
- Des visites de la pépinière de quartier et du poulailler par les écoliers, les résidents d'EHPAD, des associations et structures pour personnes handicapées 
- Des événements conviviaux en fonction des saisons : bourses aux plantes et aux graines, fêtes des voisins, permis de végétaliser, restitution de compost...

A bientôt pour construire ensemble notre ville de demain !