---
title: La conserverie
picture: "/uploads/Picto-ConserverieLocale.png"
category: Agriculture, alimentation
description: 'Les Fermiers d''ici, en partenariat avec des agriculteurs lorrains,
  tous engagés en Agriculture Biologique (AB), mitonnent de bons plats afin que les
  habitants du territoire puissent profiter des produits de notre terroir. Après avoir
  créé un food truck, puis un service traiteur, ils développent maintenant, avec l''aide
  de la Serre à projets, une conserverie. '
weight: 1
etude_opportunite: ''
nom_orga_porteur: Les Fermiers d'ici
nom_contact_porteur: Franck Magot
tel_porteur: 06 52 00 29 04
mail_porteur: franckmagot@fermiersdici.com
adresse_porteur: ''
ville_porteur: Houdemont
site_web_porteur: https://www.lesfermiersdici.com
photo: ''

---
Les Fermiers d'ici, en partenariat avec des agriculteurs lorrains, tous engagés en Agriculture Biologique (AB), mitonnent de bons plats afin que les habitants du territoire puissent profiter des produits de notre terroir. Après avoir créé un food truck, puis un service traiteur, ils développent maintenant une conserverie. A ce jour, l'entreprise propose cinq recettes dont deux vegan : tajine d'agneau, sauté de veau, petit salé aux lentilles, curry végétarien, risotto au quinoa et pleurotes.

Vous pouvez trouver nos plats directement sur le site de l'entreprise et bientôt dans des magasins revendeurs. Les Fermiers d'ici souhaitent obtenir le label bio, afin que leurs produits soient commercialisés dans des magasins spécialisés comme biocoop. Une autre envie est de proposer ces bocaux aux restaurants, bars et hôtels de la région. 

L'entreprise proposera également à ses agriculteurs partenaires de la prestation de services. Les Fermiers d'ici s'occuperont de la transformation, et ces agriculteurs pourront vendre leurs bocaux sous leur propre marque.

Enfin, afin qu'un maximum de personnes profitent de ses bocaux bio et locaux, l'entreprise souhaite développer une gamme de produits à destination de la restauration collective., en espérant que ce mode de production permette de réduire les coûts et ainsi proposer ces produits de qualité à un prix abordable.