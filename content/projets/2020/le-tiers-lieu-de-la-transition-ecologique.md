---
title: Le tiers lieu de la transition écologique
picture: "/uploads/Picto-TiersLieux-Transition.png"
category: Économie et consommation
description: Les tiers lieux sont des espaces de vie, de travail et de partage qui
  favorisent les projets en commun et les fertilisations croisées. En ce sens, un
  tel lieu thématisé sur la transition et implanté en région nancéienne serait profitable
  à tout les acteurs de la société civile qui peu à peu s'engagent sur le sujet. On
  peut ainsi imaginer un tiers Lieu sur un site emblématique, en zone rurale ou urbaine,
  accueillant entreprises, associations, porteurs de projets et grand public, au service
  d’une effervescence de projets, de rencontres et de transferts de connaissance.
  Il aurait également vocation à être un lieu de médiation autour de la transition
  pour le grand public.
weight: 1
etude_opportunite: "/uploads/Etude_Tiers_lieu_de_la_transition_ecologique.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''
site_web_porteur: ''

---
