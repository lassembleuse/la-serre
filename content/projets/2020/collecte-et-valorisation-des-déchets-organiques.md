---
weight: 6
title: La collecte des déchets organiques
picture: "/uploads/Picto Composterie.png"
category: ''
description: Le projet de la Composterie vise à développer une activité qui repose
  sur la récupération des biodéchets - aussi appelés “déchets organiques”- et de les
  valoriser en les transformant en compost destiné à être vendu.
etude_opportunite: ''
nom_orga_porteur: La Composterie
nom_contact_porteur: Martin et Mortimer Dubois
tel_porteur: 06 87 64 48 92
mail_porteur: composterie.nancy@gmail.com
adresse_porteur: ''
ville_porteur: Nancy
site_web_porteur: ''
photo: ''

---
Le projet de la Composterie vise à développer une activité qui repose sur la récupération des biodéchets - aussi appelés “déchets organiques”- et de les valoriser en les transformant en compost destiné à être vendu.

La récolte se fait directement sur les sites producteurs, et quotidiennement si nécessaire. Les tournées sont effectuées à vélo pour limiter au maximum l'empreinte carbone du service, qui fournit des bacs propres adaptés laissés en échange des bacs remplis de déchets. Le nettoyage de ces derniers est à la charge de l'entreprise.

La vente du compost peut se faire en gros volume ou au détail. Dans le second cas, la vente se fait en vrac ou dans des contenants respectueux de l’environnement (sacs en toile de jute par exemple). Les ventes de compost se font sans ou avec très peu d’intermédiaires, toujours dans l’idée d’éviter le recours à l’exportation hors du territoire. Elles s’adressent en priorité à des acteurs qui partagent les valeurs de l'entreprise, et il est possible d’acheter le compost en monnaie Florain.

Ces modalités de vente nous permettent de favoriser le circuit court à l’échelle nancéienne. Le projet espère rallier un maximum d’acteurs locaux (restaurateurs, cantines scolaires, cantines d’entreprises, hôpitaux, boutiques de fleurs) notamment ceux relatifs à la transition alimentaire locale, en instaurant un dialogue et en leur proposant des moyens concrets d’agir pour la transition écologique.