---
title: La sensibilisation à l'alimentation responsable
picture: "/uploads/Picto-Cuisine-responsable.png"
category: Agriculture, alimentation
description: 'Le projet vise à créer un acteur de référence qualifié en matière d’alimentation
  et de consommation durables, porteur d''une offre de sensibilisation et de formation
  (initiale et continue). '
weight: 3
etude_opportunite: ''
nom_orga_porteur: ''
nom_contact_porteur: Chloé Lelarge, Anaïs Streit, Lucie Devoille
tel_porteur: 06 88 74 59 78
mail_porteur: lelargechloe@yahoo.fr
adresse_porteur: ''
ville_porteur: Nancy
site_web_porteur: ''
photo: ''

---
Le projet vise à créer un acteur de référence qualifié en matière d’alimentation et de consommation durables, porteur d''une offre de sensibilisation et de formation (initiale et continue).

Pour permettre une transition alimentaire efficace et inculquer une nouvelle culture de consommation durable au plus grand nombre, les inverventions seront menées auprès de professionnels du secteur éducatif et social ainsi que du grand public. Les prestations revêtent différents formats : ateliers, formation initiale et continue, consulting. L’approche employée mêle éducation, créativité, médiation.

L’ambition de ce projet est la mise à disposition de réponses sur les plans environnementaux, sociaux et de santé publique. Il s’agit donc d’influencer et co-construire les politiques locales d’éducation, de formation et de prévention sur les thématiques d'intervention, de participer à la structuration de filières alimentaires de proximité, d’impliquer le territoire dans la mise en place d’environnements favorables à l’adoption de pratiques durables…

Ce projet s’inscrit dans une démarche globale de modification des habitudes et des modes de vie. L’alimentation durable n’est pas synonyme d’alimentation triste, au contraire, elle peut être une “alimentation plaisir” avec des produits de qualité, divers, respectueux de l’Homme et de l’environnement. Modifier sa consommation peut être un puissant levier de modification des comportements en faveur de pratiques plus durables par le biais de l’éducation.