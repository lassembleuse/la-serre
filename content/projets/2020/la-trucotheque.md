---
title: La trucotheque
picture: "/uploads/Picto-Trucotheque.png"
category: Économie circulaire
description: Le réseau des Repair Cafés du Grand Nancy propose de mettre en place
  une Trucothèque, lieu de rencontre et de mise à disposition de matériel de base
  pour les étudiants.
weight: 5
etude_opportunite: ''
nom_orga_porteur: MJC Lorraine
nom_contact_porteur: Marie-Jo Burgun
tel_porteur: 03 83 15 90 00
mail_porteur: marie-jo.burgun@mjclorraine.com
adresse_porteur: ''
ville_porteur: Vandoeuvre-lès-Nancy
site_web_porteur: https://repairgrandnancy.fr
photo: ''

---
Nancy est une ville étudiante importante. Aux rentrées universitaires, chaque étudiant doit investir dans un équipement de base, ce qui est difficile pour les étudiants en situation de précarité financière. Le Campus de Nancy est ainsi celui qui possède l'effectif le plus important de boursiers au taux maximal. Avec les acteurs de ce projet, étudiants, professeurs, personnel administratif du campus et peut être voisins, le réseau des Repair Cafés du Grand Nancy propose de mettre en place une Trucothèque, lieu de rencontre et de mise à disposition de ce matériel de base.

Le réseau apporte dans ce projet coopératif son expertise dans :

* Le recrutement des dépan'acteurs bénévoles et dans leur formation.
* Le recrutement de l'équipe logistique
* La facilitation dans l'organisation des premiers ateliers
* L'animation des ateliers ou événements liés à la vie de cet atelier
* L'accompagnement technique et logistique
* Une participation active dans le domaine de l'ESS, de l'économie circulaire et de la solidarité.

Le projet porte sur 2 pôles : la réception et gestion de stocks des objets cédés, et la maintenance de ces objets qui vont être mis à disposition en toute sécurité aux bénéficiaires.