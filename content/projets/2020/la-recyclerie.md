---
title: La recyclerie créative de mobilier
picture: "/uploads/Picto-Recyclerie.png"
category: Économie circulaire
description: "Le projet a pour objectif la création d’une recyclerie de mobilier, sur le territoire du Grand Nancy. La vocation de cette recyclerie sera d’être un lieu de rencontres, d’échanges, et de créativité, ouvert à tous. "
weight: 4
etude_opportunite: ''
nom_orga_porteur: La Benne Idée
nom_contact_porteur: Antoine Plantier, Chloé Geiss
tel_porteur: 07 82 31 06 00
mail_porteur: aplantier.fr@gmail.com
adresse_porteur: ''
ville_porteur: Nancy
site_web_porteur: 
photo: ''

---
Le projet a pour objectif la création d’une recyclerie de mobilier, sur le territoire du Grand Nancy. La vocation de cette recyclerie sera d’être un lieu de rencontres, d’échanges, et de créativité, ouvert à tous. Les volets artistiques et sociaux s’inspireront du patrimoine local, en particulier des valeurs de l’Ecole de Nancy.

Les missions de la recyclerie seront : 
- **Environnementale** : prévention et réduction des déchets ;
- **Sociale** : rencontres, échanges, prix réduits, lien social, mixité ;
- **Economique** : création d’emplois et d’activité non   délocalisables ;
- **Créative** : design et création de mobilier. 

Les activités de la recyclerie seront regroupées en deux pôles: 

### Pôle Production
- Design 
- Collecte de matériaux et mobiliers destinés à être jetés ; 
- Tri et remise en état des objets collectés ; 
- Rénovation/upcycling pour la création de nouveaux meubles et objets décoratifs ; 
- Vente de matériaux et mobilier issus du réemploi ; 

### Pôle Service :
- Ateliers de sensibilisation à la réduction des déchets et au développement durable ; 
- Ateliers et formations à la rénovation; 
- Atelier de cohésion d’équipe ; 
- Location d’outils et d’espaces ; 
- Café/bar 

Ces activités et services seront ouverts aux particuliers, étudiants ainsi qu’aux entreprises et collectivités. L’ensemble de ces aspects seront validés avec les organismes publics de référence et une dynamique citoyenne devra être créée.