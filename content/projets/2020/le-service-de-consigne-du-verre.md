---
title: Le service de consigne du verre
picture: "/uploads/Picto-Services-Consignes.png"
category: Économie et consommation
description: 'La question de la consigne est de plus en plus à l’ordre du jour des
  pouvoirs publics, notamment avec la loi « Economie circulaire » en cours de discussion.
  L’idée est ici de relancer sur le territoire un service de consigne pour un certain
  nombre de produits locaux conditionnés dans du verre : bière, vin, jus, huile, confiture...
  La consigne pourra également travailler à une maille plus large en coopération avec
  d’autres services identiques sur d’autres territoires, permettant de structurer
  des filières de collecte efficientes, et s''ouvrir à d''autres types de contenants
  (bento, eco-cup, etc...).'
weight: 7
etude_opportunite: "/uploads/Etude_Developpement_activite_de_service_de_consigne.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''
draft: true


---
