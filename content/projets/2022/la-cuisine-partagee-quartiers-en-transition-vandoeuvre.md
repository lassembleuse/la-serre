---
weight: 7
title: La cuisine partagée [Quartiers en transition - Vandoeuvre]
picture: "/uploads/picto-cuisine-partagee.png"
category: ''
description: |-
  Une cuisine partagée est un lieu qui propose la location d’espaces de travail ouverts aux professionnels, particuliers et/ou associations. On parle aussi de cookworking ou coworking culinaire où l’ambiance peut y être à la fois appliquée et conviviale. Ce type de projet peut participer au renforcement du lien social et intergénérationnel et aider
  certains publics à retrouver confiance en soi et pouvoir d’agir. Aussi, de manière générale, il existe un besoin de cuisine professionnelle pour permettre aux traiteurs bio et solidaires mais aussi certaines associations de réaliser leurs productions.
etude_opportunite: "/uploads/laserre_etude_opportinite_cuisine_partagee_vandoeuvre.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''
draft: true

---
