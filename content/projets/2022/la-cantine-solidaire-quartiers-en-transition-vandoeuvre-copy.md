---
weight: 6
title: La cantine solidaire
picture: "/uploads/picto-cantine-solidaire-1.png"
category: ''
description: Vert T'y Go, via son projet Yummy Box, souhaite proposer une restauration
  équilibrée, saine et à moindre frais
etude_opportunite: "/uploads/laserre_etude_opportinite_cantine_solidaire_vandoeuvre.pdf"
nom_orga_porteur: Vert T'y Go !
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
Yummy Box est un concept de container aménagé en cuisine. Il permettra aux bénéficiaires d’avoir accès à une alimentation équilibrée, saine et à moindre frais. Une cuisine du monde y sera également proposée, préparée par tous et pour tous pour y découvrir les saveurs étrangères alliées aux produits de la région. En outre, des ateliers y seront dispensés autour de thématiques telles que la lutte contre le gaspillage ou la transformation de produits distribués.

L’ambition de l'association Vert T'y Go!  est la création d’une place associative d’accueil pour et avec les déracinés (demandeurs d’asile, réfugiés, personnes isolées) et les enracinés (producteurs, associations, artistes et artisans locaux) réunissant différents containers aménagés : une cuisine, un espace d’exposition, un lieu de création.