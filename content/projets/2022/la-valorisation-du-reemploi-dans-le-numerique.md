---
weight: 1
title: " La valorisation du réemploi dans le numérique"
picture: "/uploads/picto-reemploi-numerique.png"
category: ''
description: La volonté est de créer une filière sur le territoire pour identifier
  des matériels numériques pouvant être reconditionné et utilisés pour les pièces
  détachées, puis distribuer à différents publics (personnes en situation de précarité,
  étudiants, personnes sensibles à la consommation responsables…).
etude_opportunite: ''
nom_orga_porteur: ULIS & SOS FUTUR
nom_contact_porteur: Pierre CLAUDE & Martin THIRIAU
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
Le projet est né suite à une réunion du PTCE sur la mise en place d’une filière de réemploi de matériel numérique. Il répond à des besoins locaux : vulnérabilité numérique, manque d’autonomie et d’équipements, et à la volonté de créer un projet collaboratif autour du réemploi.

Le projet se développerait autour de 3 axes principaux :

1\. Réemploi de matériel informatique : récupération, reconditionnement et distribution.

2\. Logiciels : favoriser l’utilisation de solutions libres et éthiques pour faire face à l’obsolescence.

3\. Auto-réparation : faire naître et entretenir une culture du réparable