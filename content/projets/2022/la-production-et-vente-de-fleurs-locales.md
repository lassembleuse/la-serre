---
weight: 8
title: La production et vente de fleurs coupées locales
picture: "/uploads/picto-fleurs-a-detourer.png"
category: ''
description: |-
  Projet de micro-ferme florale biologique cultivant exclusivement des fleurs naturelles fraiches et séchées (en hiver), locales et de saison. Il s’agirait de la 1 ère ferme de ce type en Meurthe-et-Moselle. Il est envisagé de la vente en circuit court localement, essentiellement à destination des particuliers. Le projet souhaite promouvoir une
  culture vertueuse et une production raisonnée.
etude_opportunite: "/uploads/laserre_etude_opportinite_fleurs.pdf"
nom_orga_porteur: Ferme Florare du Sânon
nom_contact_porteur: Caroline THINUS
tel_porteur: 06.86.47.29.67
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
"Cultiver des fleurs oui, mais en restant actrice de la transition écologique!" 

Tel est le credo de l'entrepreneuse qui souhaite réduire au maximum son impact négatif sur l'environnement et maximiser son impact positif sur la biodiversité et la valorisation du territoire. Elle souhaite ainsi créer une ferme florale qui soit éthique, à échelle humaine et exemplaire du point de vue environnemental.

Cette activité ce sera :

* Une micro ferme florale : une petite surface mais une production dense, une rotation et une succession des cultures et une terre toujours cultivée ou presque (engrais vert en hiver).
* Une ferme à taille humaine dimensionnée pour une personne au démarrage.
* Uniquement des fleurs naturelles fraîches et séchées (en hiver), locales et de

  saison.
* La première ferme florale consacrée à la fleur coupée en Meurthe et Moselle
* Des fleurs ultra fraîches : cueillies le matin, vendues l’après-midi ou le lendemain matin, pas de chambre froide tout au plus un stockage en cave bien fraîche. Cueillette au maximum sur commande pour éviter le gaspillage et le temps perdu.
* Une ferme en agriculture biologique, aucun intrant chimique, aucun pesticide 