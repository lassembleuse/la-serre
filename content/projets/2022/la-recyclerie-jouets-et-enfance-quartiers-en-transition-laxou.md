---
weight: 0
title: La recyclerie jouets et enfance [Quartiers en transition - Laxou]
picture: "/uploads/picto-recyclerie-enfant.png"
category: ''
description: D’après l’ADEME, en 2017, 157 000 tonnes de jouets ont été mis sur le
  marché dont 90% sont importés. Le gisement de déchets annuel est estimé à 100 000
  tonnes. Quant au marché de la puériculture, les résultats sont tout aussi affligeants.
  L’objet de ce projet est donc d’identifier des activités économiques à même de renforcer
  principalement le réemploi des jouets et des autres équipements liés à l’enfance.
etude_opportunite: "/uploads/laserre_etude_opportinite_ressourcerie_laxou.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''
draft: true

---
