---
weight: 11
title: Le tiers-lieu de la transition [Quartiers en transition - Laxou]
picture: "/uploads/picto-tierslieux-transition-qpv.png"
category: ''
description: L'association "Si l'on se parlait" porte le  projet de tiers-lieu de
  la transition écologique qui est né d’une volonté de sensibiliser les citoyens aux
  préoccupations environnementales et à cette situation de surconsommation des ressources
  naturelle sur le quartier "Les Provinces" à Laxou. Dans le cas présent, le projet
  aura avant tout comme objectif l’innovation sociale et la transition écologique
  au sens large. Il développera le « faire ensemble écoresponsable » et retissera
  des liens.
etude_opportunite: ''
nom_orga_porteur: Association Si l'on se parlait
nom_contact_porteur: M. HAFIANE Ouardi (Coordinateur)
tel_porteur: 06 87 37 26 32
mail_porteur: silonseparlait@gmail.com
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: Laxou
photo: ''

---
Créée en mars 2002, l’association ‘’Si l’On Se Parlait !’’ a pour objectif de créer du lien entre des personnes partageant un même espace de vie quelles que soient leurs différences. L’association souhaite mettre en place un lieu inspiré de la « Recyclerie » de Paris, axé sur le lien social et la transition écologique. Cet espace disposerait à terme :

\- une recyclerie

\- un espace convivial (jeux)

\- un espace pour la rénovation de meubles, etc.

\- une outilthèque

\- une cantine solidaire