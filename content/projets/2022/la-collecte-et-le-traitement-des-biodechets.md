---
weight: 9
title: La collecte et le traitement des biodéchets
picture: "/uploads/picto-biodechets.png"
category: ''
description: |-
  Les biodéchets représentent un tiers des poubelles résiduelles des Français. Il s’agit donc d’un gisement non négligeable qu’il est possible de collecter, trier et valoriser. Ainsi, le traitement écologique de ces déchets pourrait offrir une opportunité pour répondre aux problématiques de valorisation et de production d’énergie. Le but est de présenter des alternatives locales de collecte et de
  traitement écologique des biodéchets.
etude_opportunite: "/uploads/laserre_etude_opportinite_biodechets.pdf"
nom_orga_porteur: AEIM ADAPEI 54
nom_contact_porteur: Stéphane BESSOT
tel_porteur: ''
mail_porteur: sbessot@aeim54.fr
site_web_porteur: http://www.aeim54.fr/
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
L’association AEIM à travers son projet souhaite imaginer une solution pour la gestion optimale des biodéchets.

Le projet pourrait à termes permettre de transformer ces déchets en de nouvelles ressources : énergie verte et engrais notamment.