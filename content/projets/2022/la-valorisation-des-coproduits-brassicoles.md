---
weight: 1
title: La valorisation des coproduits brassicoles
picture: "/uploads/picto-coproduits-brassicoles.png"
category: ''
description: Le secteur brassicole génère des déchets et des coproduits liés à la
  fabrication de bières à chaque étape de sa production. Il paraîtrait donc intéressant
  d'imaginer des solutions pour revaloriser ces déchets et coproduits et ainsi accompagner
  les brasseries à réduire leur impact environnemental.
etude_opportunite: "/uploads/laserre_etude_opportinite_brassicole.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''
draft: true

---
