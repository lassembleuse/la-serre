---
weight: 5
title: Le café des enfants
picture: "/uploads/picto-cafe-des-enfatns.png"
category: ''
description: |-
  Création de 2 premiers Cafés des Enfants (un sur le quartier des trois maisons à Nancy et un sur Lunéville). Ces lieux sont envisagés comme des espaces de partage, d'accueil et d’échange, où enfants et parents peuvent venir se ressourcer un peu “comme à la maison”. Cet espace participatif est dédié à l’enfant (du bébé à l’adolescent) et à sa famille dans l’optique de prendre soin du vivant
  en favorisant le lien social, l’échange et la communication. Les porteurs de projets ont pour ambition de développer un réseau de café sur le département de la Meurthe-et-Moselle.
etude_opportunite: ''
nom_orga_porteur: Le café des enfants
nom_contact_porteur: Magali CURE & Charlotte RAVARY
tel_porteur: 0676635614 - 0761756112
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
Le projet de café des enfants vise à soutenir la parentalité en permettant aux parents de se ressourcer et en leur offrant un espace de repos et de découverte sécurisant pour leurs enfants. Il permet aussi aux enfants d’investir un lieu adapté à leurs besoins en termes de motricité, de développement de l’autonomie mais aussi de sécurité et de développement de la communication, de la socialisation.

L’idée première est de développer un lieu de type « café » avec une infrastructure et du matériel adaptés pour que les parents et leurs enfants puissent venir y boire un verre et se restaurer sans se soucier de l’intendance liée à la vie avec des enfants !

Mais au-delà de cette idée, l’envie profonde est de pouvoir créer un lieu qui soit vivant, modulable, amenant du lien et qui porte des valeurs écologiques.

Le lieu sera composé de plusieurs espaces :

\- Un parking à poussettes et un coin vestiaire. Chacun pourra ainsi laisser ses affaires personnelles (les sacs à langer souvent encombrants) dans le vestiaire (sécurisé).

\- Un espace petite restauration :

o Pour les tout-petits avec leur accompagnant : petits salons (fauteuils bas et tables basses (rondes pour éviter les coins de tables ! 😉 ) pour que les accompagnants soient à hauteur des bébés. Des peaux de moutons seront disposées pour que les bébés puissent se mouvoir librement. Des arches d’éveil pourront être disposées. Des transats seront également disponibles sur demande pour les personnes moins ouvertes à la motricité libre. Pour les plus grands, des petites chaises seront également à disposition.

o Plusieurs grandes tables (qui pourront être rassemblées pour en créer une très grande pour les événements) permettront aux enfants plus grands et aux accompagnants de prendre leur collation.

\- Un coin ludothèque des tout-petits (avec hochets, matériel d’éveil) où les accompagnants pourront venir emprunter du matériel pour les bébés.

\- Un coin jeux pour les enfants plus grands. (Jeux de construction, d’imitation, déguisements, dessin, puzzle etc.)

\- Un coin repos, isolé phoniquement pour permettre aux enfants d’aller se reposer. Un espace lecture viendra créer un lien entre l’espace animé et l’espace repos.

\- Une salle dédiée aux ateliers.

\- Un espace dépôt-vente et/ou recyclerie (selon l’espace du local disponible) permettant de favoriser la seconde main.

\- Des sanitaires adaptés avec une table à langer, des W.C. et lavabos bas pour enfant.

\- Si possible un espace extérieur pour jardiner, s’aérer, créer