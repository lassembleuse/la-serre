---
weight: 7
title: La recyclerie du textile
picture: "/uploads/picto-recyclerie-du-textile.png"
category: ''
description: Création d'une association d’insertion sociale à destination des femmes
  isolées et éloignées de l’emploi, par le biais d’une activité de recyclage des textiles
  usagés. Il s’agira d’une recyclerie créative de textile qui promouvra la consommation
  responsable au travers d’actions de sensibilisation, de la collecte, de la valorisation
  (production couture et retouche) et de la vente. Puis, le développement en parallèle
  d’une activité écologique de lavage du linge, afin de laver le textile collecté
  avant de le recycler.
etude_opportunite: ''
nom_orga_porteur: Recyclerie les 3 R
nom_contact_porteur: Natacha GUIDET
tel_porteur: 06.26.66.05.48
mail_porteur: contact@recyclerieles3r.org
site_web_porteur: ''
adresse_porteur: L'Octroi
ville_porteur: Nancy
photo: ''

---
Il s’agira d’une recyclerie créative de textile, c’est à dire un lieu dans lequel la structure proposera de la sensibilisation, de la collecte de déchets textile, de la valorisation, et de la vente.

La valorisation passera par le nettoyage (dans notre atelier d’entretien écologique du linge), par la réparation ou la transformation des vêtements ainsi que par la création.

L'objectif étant d'y fabriquer un maximum de choses à partir d'éléments collectés à la Recyclerie.

Notre mission serait à la fois sociale, puisque nous accompagnerons des personnes éloignées de l’emploi en leur proposant un accompagnement socio professionnel, une mission de formation, et une mission environnementale.

Pourquoi les 3 R ?

* Réduire les déchets textiles et leur impact sur l’environnement
* Réutiliser le textile collecté pour lui donner une seconde vie
* Recycler les ressources en de nouvelles formes ou matières et valoriser les déchets