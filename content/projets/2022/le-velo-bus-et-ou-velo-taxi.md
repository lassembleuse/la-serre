---
weight: 3
title: Le vélo-bus et/ou vélo-taxi
picture: "/uploads/picto-velobus-taxi.png"
category: ''
description: En 2017, les transports étaient responsables de 30 % des émissions de
  gaz à effet de serre de la France. 95 % de ces gaz à effet de serre sont dus au
  transport routier, dont 56 % aux seules voitures. Or, 40 % des déplacements en voiture
  font moins de 3km, tout en sachant que la majorité (58%) des trajets entre le domicile
  et lieu de travail sont inférieures à un kilomètre. Il paraît donc important de
  trouver des alternatives pour réduire la congestion dans les villes et les émissions
  de gaz toxiques. C’est ici que le vélo-bus et le vélo-taxi peuvent apporter une
  solution intéressante.
etude_opportunite: "/uploads/laserre_etude_opportinite_velobus_taxi.pdf"
nom_orga_porteur: Stanislas et Diabolo
nom_contact_porteur: Eric JOFFRES & Maxime KAUFFMANN
tel_porteur: 06.34.14.60.08
mail_porteur: eric.joffres@dynamo-location.fr
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: Nancy
photo: ''

---
Le porteur de projet souhaite développer une offre de vélo-taxi et vélo-bus en cohérence avec l’étude menée par la Serre à projets. En effet, l’offre de vélo-taxi serait adressée aux touristes, rendez-vous d’affaires et aux personnes à mobilité réduites. Quant au vélo-bus, le porteur de projet souhaite proposer une offre sur l’accompagnement à la fourniture des matériels ainsi qu’une offre sur l'accompagnement à la formation des personnels utilisateurs et qui pourrait cibler les écoles.