---
weight: 2
title: La solution d'auto-hébergement libre et écologique
picture: "/uploads/picto-la-grappe.png"
category: ''
description: Codatte a créé un cabinet de développement qui a souhaité engager une
  réflexion sur les méthodes d’hébergement classiques. C'est ainsi qu'est né le projet
  "La Grappe", une solution d'auto-hébergement qui assure souveraineté des données
  et des services tout en réduisant considérablement l'empreinte énergétique.
etude_opportunite: ''
nom_orga_porteur: Codatte
nom_contact_porteur: Théo HUNERBLAES & Piet ROSE
tel_porteur: 06.59.11.10.64 - 06.71.64.59.14
mail_porteur: theo@codatte.fr
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
Société fondée en 2020, Codatte est la mise en commun de projets de long terme, fruits d’une vision commune ; celle de partager outils et savoir-faire de l’informatique Libre auprès du plus grand nombre. L’entreprise est motivée par l’accession aisée à la technologie, la liberté et la diversité offerte par les logiciels ouverts et connectés.

Les porteurs de projets souhaitent aujourd’hui fournir des solutions d’auto-hébergement via leur projet “La Grappe”. L’idée est d’offrir la souveraineté des données et des outils de travail.

Avec la Grappe, les services en ligne sont localisés où le client le désire, via n'importe.s quelle connexion.s internet. Les données et les services restent accessibles en toute sécurité depuis n’importe quel appareil.

Aussi, le système numérique est un aspect de nos sociétés de consommation qui fait peu l’objet de débats dans la transition énergétique. La Grappe souhaite déployer des infrastructures technologiques plus sobres et qui permettent aux organisations de travailler dans de bonnes conditions avec une empreinte réduite, ceci par le biais du reconditionnement d'ordinateurs bureautiques en serveur, ou par l'utilisation de serveur à basse consommation électrique.

Relocaliser les données produites par des établissements partenaires, avec un profil de structure de taille variées (PME, Collectivités, Associations, Indépendants, libéraux, etc).

En se plaçant proche de la source de données, les porteurs de projet ont pour ambition de limiter les intermédiaires et les transferts lourds vers des infrastructures énergivores et délocalisées, hors-sol. Limiter l’utilisation de services “cloud" installés en Data-Centres est un premier pas vers une forme de sobriété et de souveraineté numérique.

La solution se présenterait sous la forme de petits boîtiers, placées sur différentes connexions internet choisies et contenant chacun un ou plusieurs micro-ordinateurs à faible consommation électrique et / ou reconditionné.

Ils forment ensemble une grappe évolutive de serveurs fournissant des logiciels “cloud" et open-source (ERP, Site web, Nextcloud, Jisti etc…).

Codatte vous accompagne lors de l’installation, la formation et l'utilisation de la Grappe.

Un plan de sauvegarde est établi lors de l’installation, vous permettant d'avoir un niveau de sauvegarde adapté à vos besoins et de réduire à zéro le risque de perte de données en cas d’incident.

L'offre de maintenance assure une intervention rapide en cas d’indisponibilité de votre Grappe, détectable par nos serveurs d’alertes.

Codatte peut prendre en charge l’évolution de votre infrastructure; en terme de performance, de fonctionnalités et de personnalisation avancées.

Par exemple, ceci permet au services de votre Grappe d’être conformes à votre identité visuelle où même répondre à des besoins métiers très précis !