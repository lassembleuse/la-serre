---
weight: 4
title: Le centre de médiation équine
picture: "/uploads/picto-mediation-equine.png"
category: ''
description: Création d'un centre de médiation équine à destination de tous les publics
  (personnes en situation de handicap et personnes valides (adultes, enfants et personnes
  âgées)).
etude_opportunite: ''
nom_orga_porteur: ''
nom_contact_porteur: Emilie ADET
tel_porteur: 06.82.58.79.39
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
Les activités proposées seraient variées : Equicie, equimotricité, hippothérapie, ergothérapie avec le cheval, promenade en saddlechariot, possibilité de prendre en pension certains des équidés, création du lien mère-enfant, etc.

L’objectif est de créer une écurie active, concept d’hébergement pour les chevaux qui s’inspire de leur mode de vie en liberté. Il permet d’associer le bien-être des chevaux à une écurie bien pensée et pratique. Cela permet d’optimiser le temps et la pénibilité du travail.

La création d’une écurie active permettra de développer de multiples projets :

\- Bien-être des humains et des animaux,

\- Découverte de la nature et de l’environnement à l’aide du cheval,

\- Lieu de détente et de ressourcement.