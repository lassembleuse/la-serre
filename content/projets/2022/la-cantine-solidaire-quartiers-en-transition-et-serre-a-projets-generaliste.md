---
weight: 10
title: La cantine solidaire [Quartiers en transition Vandoeuvre]
picture: "/uploads/picto-cantine-solidaire-qpv.png"
category: ''
description: "L’association Khamsa souhaite mettre en place un espace, en favorisant
  l’insertion des femmes du quartier, avec :\n    • Restauration solidaire\n    •
  Service traiteur\n    • Animations culturelles "
etude_opportunite: ''
nom_orga_porteur: Association KHAMSA
nom_contact_porteur: Abdellatif Mortajine, Président
tel_porteur: '09 54 24 17 82'
mail_porteur: association.khamsa@free.fr
site_web_porteur: http://association-khamsa.blogspot.fr
adresse_porteur: 1 allée de Breda 54500 Vandoeuvre
ville_porteur: Vandoeuvre
photo: ''

---
Créée en 2000, l’association Khamsa a débuté en tant que « traiteur associatif » en favorisant l'insertion professionnelle des femmes issues de l'immigration et en valorisant ainsi leur savoir-faire culinaire. Quelques années plus tard, l’association a diversifié ses domaines d’intervention tout en réduisant en parallèle l’activité de traiteur. Aujourd’hui l’association souhaite à nouveau travailler autour de la thématique de l’alimentation et du lien social et propose un projet intégrant :

* Restaurant sociale, solidaire et écologique
* Lieu de rencontres, d’information et de formation informelle
* Espace d’expression culturelle et artistique