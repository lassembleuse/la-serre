---
weight: 4
title: La galerie durable
picture: "/uploads/picto-galerie2.png"
category: ''
description: |-
  Une galerie durable permettrait de regrouper en son sein divers acteurs de
  l’Economie Sociale et Solidaire du territoire Sud Meurthe-et-Mosellan et
  représenterait ainsi une réelle alternative aux galeries marchandes traditionnelles.
  On pourrait y retrouver, entre autres, du vêtement, de l’alimentaire, du
  meuble, de l’électroménager, des produits high-tech reconditionnés, etc.
  La galerie durable a pour mission de permettre aux acteurs de l’Economie Sociale et Solidaire de gagner
  en visibilité et d’accroître leur chiffre d’affaires, tout en offrant la possibilité au citoyen-consomm’acteur de se fournir en produits locaux,
  éthiques et responsables en un seul et même endroit.
etude_opportunite: "/uploads/etude-galerie-durable.pdf"
nom_orga_porteur: ''
nom_contact_porteur: ''
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''
draft: true

---
