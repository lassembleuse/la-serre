---
weight: 5
title: Le service de livraison à vélo
picture: "/uploads/picto-cyclo.png"
category: ''
description: Les Coursiers Nancéiens sont aujourd'hui organisés en une association
  consituée de coursiers indépendants ayant pour projet de fonder une coopérative
  de coursiers à vélo sur la Métropole nancéienne.
etude_opportunite: "/uploads/etude-cyclologistique.pdf"
nom_orga_porteur: Les Coursiers Nancéiens
nom_contact_porteur: ''
tel_porteur: 07 49 57 57 59
mail_porteur: contact@lescoursiersnanceiens.fr
site_web_porteur: https://lescoursiersnanceiens.coopcycle.org/fr/
adresse_porteur: ''
ville_porteur: Nancy
photo: ''

---
Les Coursiers Nancéiens sont aujourd'hui organisés en une association consituée de coursiers indépendants ayant pour projet de fonder une coopérative de coursiers à vélo sur la Métropole nancéienne. Initiative locale, éthique, solidaire et qualitative.

La mise en place d’une activité de service de logistique à vélo vise à la création d’une coopérative réunissant des coursiers à vélo permettant la distribution de toutes sortes de plis et colis à destination des particuliers et des entreprises. La coopérative propose également la prise en charge de colis volumineux via l’utilisation de vélo utilitaires, biporteurs, triporteurs et/ou vélos cargos à assistance électrique.

Le service proposé aura pour vocation de proposer une réelle alternative à l’uberisation et ainsi valoriser les emplois des livreurs, ainsi que d'optimiser écologiquement et organisationnellement la logistique du dernier kilomètre.