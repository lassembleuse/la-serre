---
weight: 6
title: Le service de consigne du verre
picture: "/uploads/picto-services-consignes-1.png"
category: ''
description: Echogestes souhaite fournir aux organisateurs un service de location,
  collecte et nettoyage d’écocups, mais aussi aux restaurateurs un service de consigne
  du verre. C’est là en effet un point noir des concerts et autres festivals que le
  sujet des contenants des boissons. Ils ont ainsi pour envie de développer des projets
  de mise en place de centrales de lavage, ou de créer des stations mobiles de nettoyage.
etude_opportunite: ''
nom_orga_porteur: Echogestes
nom_contact_porteur: Guillaume Thomas
tel_porteur: ''
mail_porteur: contact@echogestes.org
site_web_porteur: http://echogestes.org
adresse_porteur: 75 Avenue du XX Corps
ville_porteur: Nancy
photo: ''

---
Echogestes souhaite fournir aux organisateurs un service de location, collecte et nettoyage d’écocups, mais aussi aux restaurateurs un service de consigne du verre. C’est là en effet un point noir des concerts et autres festivals que le sujet des contenants des boissons. Ils ont ainsi pour envie de développer des projets de mise en place de centrales de lavage, ou de créer des stations mobiles de nettoyage.

Ainsi, le projet de consigne pourrait intégrer plusieurs aspects : gobelets réutilisables dans le cadre d’événements festifs (1ère expérimentation dans le cadre de l’Eté indien en sept. 2020), des bouteilles, bocaux et verrines (la station de lavage des gobelets pourra être proposer aux producteurs locaux), des consignes en verre pour les restaurateurs, traiteurs et la vente à emporter.

L’association Echogestes œuvre à la réduction des déchets depuis plusieurs années sur le 54 (repair-cafés, ateliers DIY, actions de sensibilisation dans les écoles, les associations, les entreprises, opérations de nettoyage, actions d’éco-préconisation sur l’ensemble des manifestations du Grand Nancy et du Sud Meurthe-et-Mosellan). Echogestes est également à l’initiative de deux collectifs portant sur la valorisation des mégots et la valorisation des masques chirurgicaux.