---
weight: 2
title: La ville verte
picture: "/uploads/picto-ville-verte.png"
category: ''
description: 'Le projet soutenu par Des Racines et des Liens se déploie en 3 axes
  qui sont complémentaires : La transformation écologique et participative des friches
  urbaines, la formation des jardiniers et agents communaux, et le développement des
  serres de production, dans une dynamique d’économie circulaire.

'
etude_opportunite: "/uploads/etude-ville-verte.pdf"
nom_orga_porteur: Des Racines & des Liens
nom_contact_porteur: Enzo Arcidiacono et Théo Guell
tel_porteur: 06 61 03 79 82
mail_porteur: enzo@desracinesetdesliens.fr
site_web_porteur: https://www.desracinesetdesliens.fr/
adresse_porteur: 51, rue du Colombier
ville_porteur: 54610 Clémery
photo: ''

---
Le projet soutenu par Des Racines et des Liens se déploie en 3 axes qui sont complémentaires :

\- La transformation écologique et participative des friches urbaines dans le but de créer des îlots de verdure, de fraîcheur et de biodiversité aux seins des villes de l’agglomération de Nancy.

\- La formation des jardiniers et agents communaux avec des modules de formation adaptés aux différents contextes. Les jardiniers pourront donc par la suite accompagner les résidents, associations dans l’aménagement et/ou le suivi des friches urbaines.

\- Repenser les serres de production, dans une dynamique d’économie circulaire, en proposant des modules de formations réservés aux jardiniers de la serre afin de produire des plants adaptés aux différents contextes (fixateur d’azote, mellifère, nectarifère…) et une ouverture des serres pour la production de plants à destination de la transformation des friches urbaines