---
weight: 
title: La valorisation du réemploi dans le bâtiment
picture: "/uploads/picto-recyclerie-du-batiment.png"
category: ''
description: "L’association Remise a pour projet de développeler le réemploi des matériaux
  de construction. Notamment en développant 3 axes : \n- la promotion et la sensibilisation
  des acteurs de la construction au réemploi\n- la structuration d’une filière du
  réemploi à l’échelle de la Meurthe-et-Moselle\n- l’accompagnement des professionnels
  et des porteurs de projets"
etude_opportunite: ''
nom_orga_porteur: Association REMISE
nom_contact_porteur: Émilie Lemoine, Christelle Hopfner-Vuille et Florent Collin
tel_porteur: ''
mail_porteur: contact@re-mise.fr
site_web_porteur: https://www.re-mise.fr/
adresse_porteur: 10 rue de Graffigny
ville_porteur: 54000 NANCY
photo: ''

---
L’association Remise est composé d’un petit groupe de passionné.es de la construction s’intéressant au réemploi des matériaux de construction.

Le projet répond aux objectifs de la loi AGEC (objectifs de prévention des déchets et de développement du réemploi et de la réutilisation prévus à l'article L.541-10). Il s’agit de contribuer au développement de la filière du réemploi de matériaux et de composants de construction.

Faciliter une économie circulaire vertueuse, une économie du partage et des modes de consommations alternatifs, qui évitent le gaspillage et la production de déchets dans la construction.

La stratégie du projet tourne autour de 3 axes :

\- la promotion et la sensibilisation des acteurs de la construction au réemploi

\- la structuration d’une filière du réemploi à l’échelle de la Meurthe-et-Moselle en recensant et fédérant les acteurs et les initiatives locales et en facilitant les connexions et les temporalités entre démolitions, chantiers et ressourceries.

\- l’accompagnement des professionnels et des porteurs de projets