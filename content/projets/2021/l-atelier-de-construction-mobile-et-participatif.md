---
weight: 4
title: L'atelier de construction mobile et participatif
picture: "/uploads/picto-atelier-mobile.png"
category: ''
description: L’idée est de créer un dispositif complet qui permet l’installation d’un
  espace de construction participatif temporaire avec des éléments conçus pour être
  transportés et déplacés facilement.
etude_opportunite: ''
nom_orga_porteur: Lacagette
nom_contact_porteur: Léo SCHEER et Tanguy POURRY
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: Nancy
photo: ''

---
L’objectif de cet atelier est de créer un dispositif complet qui permet l’installation d’un espace de construction temporaire.

Les porteurs du projet imaginent un atelier entièrement mobile avec des éléments conçus pour être transportés et déplacés facilement. Ce projet permettrait de se déplacer sur différents lieux, et servirait à fabriquer in situ des espaces, du mobilier, des objets.

Le projet montre l'importance d’apporter des espaces où les savoirs s’échangent, et surtout où la pratique est considérée en tant que telle. Les porteurs de projet souhaitent offrir la possibilité à différents publics de venir s’exercer, se dépasser, et concrétiser une idée par une forme.

L’objectif est vraiment porté sur la visée pédagogique et la transmission des compétences utiles liées à l’auto-construction. Ces moments collectifs veulent générer des ambitions communes, des envies partagées. Nombreux citoyens ont des difficultés pour se rendre acteur de leur ville. Ces nouveaux temps sont les espaces appropriés à l’imaginaire collectif.

Avec cet outil, le projet peut donner la capacité à d’autres porteurs de projets de faire appel au collectif Lacagette pour la construction de mobiliers ou d’éléments utiles au lancement de leur activité. Il est également possible d'apporter une approche pratique et technique pour la réalisation de prototype puis d’un produit fini. Il s’agit là de créer des synergies entre acteurs de la serre pour la concrétisation des projets.