---
weight: 7
title: La coopérative funéraire
picture: "/uploads/picto-coop-funeraire.png"
category: ''
description: |-
  Projet de coopérative funéraire éthique et écologique sur le secteur de Nancy et de son agglomération. Au-delà d’un service funéraire, la porteuse souhaite proposer un lieu d’accueil et d’échanges, un service de conseil/information des actions de sensibilisation et un « laboratoire » pour
  travailler sur l’évolution de la loi du funéraire en France
etude_opportunite: ''
nom_orga_porteur: Le Jour d'après
nom_contact_porteur: Aurélie Didier-Laurent
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: Nancy
photo: ''

---
La porteuse de projet souhaite développer une coopérative funéraire éthique et écologique qui offre :

* Un service funéraire à des familles qui viennent de perdre un proche (accompagnement à l’organisation des obsèques dans le respect de la procédure et en prenant en compte les volontés des défunts et des familles)
* Une proposition d’accompagnement aux démarches administratives post-obsèques
* Un service de conseil/information sur différentes questions : la réglementation du funéraire
* Un « laboratoire » pour travailler à l’évolution de la loi du funéraire en France : l’humusation, une solution pour des funérailles écologiques.
* Des soirées de sensibilisation du grand public sur des sujets liés à la mort et au funéraire : ciné-discussions, « cafés mortels »
* Un espace numérique pour chaque famille des défunts pour déposer les condoléances, visionner les obsèques à distance pour les personnes qui ne peuvent se déplacer, retrouver les démarches administratives à effectuer et y déposer les documents nécessaires.