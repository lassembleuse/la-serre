---
weight: 3
title: La vélo-école
picture: "/uploads/picto-velo-ecole.png"
category: ''
description: Projet d’implantation d’une vélo-école dans l’agglomération nancéienne
  qui aura comme mission de former un public vaste composé d’enfants et d’adultes.
etude_opportunite: "/uploads/etude-velo-ecole.pdf"
nom_orga_porteur: ''
nom_contact_porteur: Aurore Coince et Hadrien Fournet
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: Nancy
photo: ''

---
Le projet vise à aider les usagers, quelque-soit leur âge, leur milieu social ou leur connaissance du sujet, à s’émanciper de la voiture individuelle ou à se déplacer plus rapidement et plus sainement en adoptant la solution vélo ou EDPM (engins de déplacement personnels motorisés).

La pratique du vélo, avantageusement complétée par les EDPM, s’adapte à la plupart des personnes dans leurs usages du quotidien. Un bon apprentissage permet une meilleure cohabitation entre les différents modes de transport et permet d’être plus efficace. Les porteurs de projet ciblent particulièrement les enfants qui adopteront plus facilement ce mode de mobilité une fois adulte s’ils ont été formés et sensibilisés. Cet apprentissage se fera généralement dans le cadre du programme gouvernemental [« Savoir rouler à vélo »](https://www.education.gouv.fr/lancement-du-programme-savoir-rouler-velo-5258 "Savoir rouler à vélo").

Aujourd’hui en France, la pratique du vélo est amenée à se développer, sous l’impulsion de nouvelles politiques locales et nationales volontaristes, qui vont investir significativement dans des infrastructures cyclables sécurisées. Les intérêts et les besoins sont nombreux sur le territoire au regard des enjeux.

Les porteurs du projet croient en un véritable changement d’échelle en faveur des mobilités actives pour nos territoires et du renforcement des liens partenariaux entre les réseaux (éducation, commerces, loisirs, tourisme,...), et avec les différentes collectivités et institutions.

Le projet souhaite également proposer de nouveaux emplois locaux, non-délocalisables, respectueux de la personne humaine en recrutant et en formant des formateurs de la pratique du vélo et/ou des EDPM .

Le projet est soutenu par trois acteurs de la mobilité douce. À savoir : les [associations EDEN](https://as-eden.org/ "associations EDEN"), [atelier Dynamo](https://atelierdynamo.fr/ "atelier Dynamo") et [VMA Grand Est](https://vma-grand-est.fr/ "VMA Grand Est").