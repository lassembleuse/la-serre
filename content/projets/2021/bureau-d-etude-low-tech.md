---
weight: 1
title: Le bureau d'étude Low-tech
picture: "/uploads/picto-lowtech-essai-3.png"
category: ''
description: Projet d’implantation d’une activité de bureau d’étude spécialisé dans
  le low-tech qui aura pour mission d’accompagner les entreprises, associations, collectivités
  et particuliers sur la réalisation de leurs projets en proposant des alternatives
  sobres en lien avec la transition énergétique et environnementale.
etude_opportunite: "/uploads/etude-bureau-d-etude-low-tech.pdf"
nom_orga_porteur: ''
nom_contact_porteur: Stéphane Haessler
tel_porteur: ''
mail_porteur: ''
site_web_porteur: ''
adresse_porteur: ''
ville_porteur: ''
photo: ''

---
Projet d’implantation d’une activité de bureau d’étude spécialisé dans le low-tech qui aura pour mission d’accompagner les entreprises, associations, collectivités et particuliers sur la réalisation de leurs projets en proposant des alternatives sobres en lien avec la transition énergétique et environnementale.