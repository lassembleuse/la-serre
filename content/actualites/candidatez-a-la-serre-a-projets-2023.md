---
date: 
title: 'Candidatez à la Serre à projets #2023 !'
picture: "/uploads/laserre_planete.jpg"
publishdate: 2023-01-05T12:34:00+01:00

---
#### C'est parti pour le lancement de [**l’appel à candidatures**](https://www.laserre.org/appel-a-candidature/ "https://www.laserre.org/appel-a-candidature/") de la Serre à projets #2023 ! Vous avez jusqu'au **12 février 2023** pour postuler !!!

Cette année encore, l'équipe propose aux futurs entrepreneurs **7 belles thématiques** qui gravitent autour de la transition écologique et solidaire :

1. [**Une activité d’optimisation des ressources en eau**](https://www.laserre.org/uploads/laserre_etude_opportunite_optimisation-des-ressources-en-eau.pdf "Ressources en eau")
2. [**Un garage retrofit**](https://www.laserre.org/uploads/laserre_etude_opportunite_garage-retrofit.pdf "garage retrofit")
3. [**Une activité d’élevage de vers et d’insectes**](https://www.laserre.org/uploads/laserre_etude_opportunite_elevage-de-vers-et-insectes.pdf "Elevage vers et insectes")
4. [**Une recyclerie du sport, loisirs et/ou jouets**](https://www.laserre.org/uploads/laserre_etude_opportunite_recyclerie-du-sport-et-loisirs.pdf "recyclerie sport et loisirs")
5. [**Une activité de développement ou renforcement de l’offre en protection et couches lavables**](https://www.laserre.org/uploads/laserre_etude_opportunite_developpement-ou-renforcement-de-l-offre-en-protection-et-couches-lavables.pdf "Couches lavables")
6. [**Une activité de valorisation des coproduits et des déchets brassicoles et de boulangeries**](https://www.laserre.org/uploads/laserre_etude_opportunite_valorisation-des-coproduits-du-secteur-brassicole.pdf "Brasseries et boulangeries")
7. [**Une activité de valorisation ou lutte contre des espèces invasives**](https://www.laserre.org/uploads/laserre_etude_opportunite_valorisation-ou-lutte-des-especes-invasives.pdf "Espèces invasives")

Vous pouvez retrouvez l’ensemble des projets de la promotion 2023 [**ici**](https://www.laserre.org/projets/2023/) !

Comme l’année dernière, les candidats auront également la possibilité de proposer leur propre projet et de se positionner sur le **volet blanc.**

### **Retrouvez toutes les informations concernant l’appel à candidatures** [**ici**](https://www.laserre.org/appel-a-candidature/ "Appel à candidature") **!**