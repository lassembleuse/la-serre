---
date: 
title: 'Revue de projets #11 : La Recyclerie les 3R'
picture: "/uploads/0-2.jpg"
publishdate: 2023-03-07T11:00:00+01:00

---
Ohé ohé, venez découvrir un projet de la [Promotion 2022](https://www.laserre.org/projets/2022/ "Promo 2022") ! Il s'agit d'une recyclerie spécialisée dans le réemploi du textile. Parcourez cet interview pour en savoir plus !

**Qui es-tu ?**

GUIDET Natacha, porteur du projet de recyclerie créative « La Recyclerie les 3R ». Je suis très sensible à l’écologie et au faire soi-même.

Mon parcours est atypique ; j’ai travaillé dans le milieu de la finance, j’ai ensuite repris des études de responsable de formations à l’université de Lorraine, et enfin j’ai créé une friperie. J’ai toujours été engagée dans diverses associations d’utilité sociale et artistique. Il manquait quelque chose à mon parcours professionnel cette envie de répondre aussi à une utilité sociale, c’est comme cela qu’est né l’envie de créer cette recyclerie créative de textile avec l’objectif de devenir chantier d’insertion et d’accompagner des personnes éloignées de l’emploi.

**Comment est né le projet de créer une association spécialisée dans le réemploi du textile ?**

Je suis passionnée de mode et soucieuse des enjeux écologiques pour notre belle planète.

Tout d’abord des chiffres :

* L’industrie du textile est la 2eme industrie la plus polluante au monde après celle du pétrole
* Elle est responsable de 20% de la pollution des eaux mondiales
* Il faut l’équivalent de 70 douches pour fabriquer 1 t shirt
* Sur 517000 tonnes de déchets textile par an en France seulement 40% sont collectés pour avoir une seconde vie

Alors si au lieu de jeter on réparait, on recyclait, on réutilisait ?

Lorsque j’ai créé ma friperie il y a 2 ans c’était avec l’idée qu’une pièce achetée dans ma boutique est une pièce qu’il était inutile de fabriquer. Après avoir réalisé un diagnostic territorial il est apparu de manière évidente un réel besoin de développer la filière de réemploi des déchets textiles sur la métropole du grand Nancy

C’est comme cela qu’est née la recyclerie les 3R.

**En quoi consistera les 3R ?**

Notre vision est de développer la création artistique pour le réemploi de déchets textiles en valorisant l’artisanat local.

Nous souhaiterions également permettre l’accès à l’emploi à des personnes en difficultés à travers un chantier d’insertion autour de la couture de la vente et du numérique.

L’objectif : les aider à développer des compétences et les accompagner vers un retour a l’emploi durable.

Nous pensons que la mode peut se conjuguer avec circulaire et solidaire.

Et nous souhaitons apporter une solution locale à un désordre global.

**Où en es-tu dans le projet ?**

Le diagnostic territorial a été établi, j’ai ensuite effectué une étude de faisabilité comprenant le dimensionnement ainsi que le modèle financier.

Nous avons commencé l’activité de collecte et de création au sein de l’Octroi où nous sommes résidents.

Nous avons signé une convention de partenariat avec l’École d’ergothérapie, des étudiants de dernières années travaillent actuellement au prototypage de vêtements adaptés au handicap à partir de vêtements de seconde main.

Nous attendons aujourd’hui de pouvoir présenter notre projet à la DEETS en espérant pouvoir obtenir l’agrément chantier d’insertion.

**Qu’est-ce qui t'attends dans les prochains mois ?**

Nous allons commencer mi-mars les marchés solidaires à l’Octroi. Ce sera un rendez-vous mensuel, un lieu où les Nancéiens pourront venir s’habiller à faible coût.

Beaucoup d’autres beaux événements et partenariats sont à venir, le calendrier sera prochainement visible sur nos réseaux (Facebook, LinkedIn et bientôt Instagram).

Nous sommes à la recherche de financements nécessaires pour développer l’activité à plus grande échelle.

Nous aurons besoin d’un local plus grand, de la participation des citoyens bénévoles et du soutien des organismes publics.

Nous espérons créer une dynamique citoyenne engagée.

  
 **Quel est l’avantage de mener cette initiative dans le cadre de la Serre à projets ?**

La Serre à projets est un réel tremplin pour la réussite d’un tel projet.

L’accompagnement est riche en formation, en conseils et permet à la fois une visibilité auprès des organismes publics mais également de mutualiser les bonnes pratiques et de créer du lien avec les autres porteurs de projets.

L’équipe de la Serre à projets est un incroyable soutien du fait de sa bienveillance et ses encouragements

**Merci !**