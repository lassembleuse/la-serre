---
date: 
title: La Fondation GRDF apporte son soutien à la Serre à projets !
picture: "/uploads/fondation_grdf_logo_cmjn-1.png"
publishdate: 2021-04-21T00:00:00+02:00

---
La [fondation GRDF](https://fondationgrdf.fr/ "fondation grdf") soutient la Serre à projets pour contribuer à faire progresser l'inclusion sociale et accélérer la transition écologique dans les territoires.

Elle note que notre projet est **"exemplaire et innovant"**. Un résultat formidable, et un nouvel acteur autour de la table des [partenaires ](https://www.laserre.org/partenaires/ "partenaires")!