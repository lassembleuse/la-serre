---
date: 
title: 7 nouvelles idées selectionnées pour la Serre à projets
picture: "/uploads/image.jpeg"
publishdate: 2020-09-14T11:00:00+02:00

---
Le Comité de Pilotage de la Serre à projets s’est réuni le mardi 8 septembre 2020 au Médiaparc à Nancy afin de procéder à la sélection des idées d’activités qui vont faire l’objet d’une **étude d’opportunité.**

Cette année, c'est la communauté [Start Up de Territoire](https://www.facebook.com/startupterritoire54/ "startupterritoire54") qui a fait émerger les 20 idées  qui ont été soumises aux membres du comité.

Après un vote et de nombreux débats entre les participants, chacun partageant ses connaissances du territoire et ses expériences, ce sont 7 idées qui ont été finalement retenues :

* **Les galeries durables :** pôle commercial à fort impact social réunissant un ensemble de commerces durables sous le même toit.
* **Service de logistique urbaine à vélo :** coopérative réunissant des coursiers à vélo permettant la distribution de toutes sortes de plis et colis. Le service proposé pourrait être une réelle alternative à l’uberisation.
* **Fonds d'investissement dans la transition écologique et solidaire :** création d'un fonds capable de prendre des participations dans les entreprises de l'économie sociale et solidaire engagées dans la transition.
* **La ville verte :** services de verdissement pour apporter de la biodiversité en ville, au service des particuliers, entreprises et collectivités.
* **Bureau d'étude low-tech :** pour la promotion de technologie plus sobre en faisant appel à un minimum de ressources.
* **La vélo-école :** pour permettre à tout le monde d'apprendre à faire du vélo en toute sécurité.
* **Opérateur logistique du bio et local :** permettre une distribution optimisée des produits bios et locaux afin de répondre qualitativement et quantitativement aux besoins des producteurs et consommateurs.

L’équipe de la Serre va maintenant réaliser des études d'opportunité pour examiner si ces idées répondent aux besoins du territoire et peuvent donner naissance à un projet.

Ces études et futurs projets seront ainsi présentés au prochain comité de pilotage en novembre. Puis, les porteurs de projet pourront se positionner sur ces idées à partir de décembre 2020 via un appel à candidatures.

Suite à la sélection des idées et au bilan de l'action 2019-2020, les porteurs de projets de la promotion actuelle ont eu la chance de présenter leur projet devant les membres du comité de pilotage. Ainsi, nous avons pu assister à la présentation de :

* **La Trucothèque,** portée par la MJC Lorraine
* **Frugali** le cabinet de conseil et de formation en pratiques durables, consommation responsable et transition alimentaire, porté par Chloé Lelarge, Lucie Devoille et Anais Streit.
* [**La Benne idée**](https://www.laserre.org/actualites/revue-de-projets-1-la-benne-idee/ "projet benne idéee")**,** la recyclerie créative, portée par Chloé Geiss, Antoine Plantier et Thomas Henzy.
* **Mimesis**, la composterie, portée par Mortimer Dubois
* La **Conserverie**, portée par les Fermiers d'Ici et son gérant Franck Magot.

S'en est suivi un pot convivial permettant aux porteurs de projets d'échanger directement avec les membres du comité de pilotage.