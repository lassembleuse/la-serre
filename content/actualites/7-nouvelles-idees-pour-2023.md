---
date: 
title: 7 nouvelles idées pour 2023 !
picture: "/uploads/idea-gf7408b1da_1280.png"
publishdate: 2022-07-19T11:30:00+02:00

---
Suite à [l'atelier capteurs d'idées](https://www.laserre.org/actualites/atelier-capteur-d-idees-cap-sur-2023/ "https://www.laserre.org/actualites/atelier-capteur-d-idees-cap-sur-2023/") le Comité de Pilotage de la Serre à projets  a tranché ! Il y aura donc **7 nouvelles idées** qui vont faire l'objet d'une étude d'opportunité :

* **Garage Retrofit :** Le rétrofit est une opération consistant à convertir une voiture thermique à l'électrique. Il remplace le moteur essence ou diesel par un moteur électrique. Autorisé en France depuis 2020, le rétrofit permet de donner une seconde vie à sa voiture. En permettant de convertir une voiture à motorisation thermique en motorisation électrique, le rétrofit permet d'accélérer le passage à l'électrique, d'augmenter le nombre de voitures électriques en circulation sur le territoire et de  diminuer la pollution émises par les transports.


* **Culture d'insectes ou de vers :** Les finalités sont diverses. L’élevage d’insectes peut être une option de diversification alimentaire chez les humains et le bétail. Aussi, il est possible d'envisager une culture de vers pour permettre la production de soie par exemple.
* **Service d'optimisation des ressources en eau :** le changement climatique va transformer la question de l’accès à l’eau en un enjeu critique. Dèslors, il devient essentiel d’optimiser l’utilisation de cette ressource, en limitant de façon très ambitieuse son gaspillage. Or, la manière dont est utilisée l’eau dans une habitation ou un bâtiment tertiaire est très largement perfectible. On pourrait ainsi imaginer l’émergence sur le territoire d’un installateur spécialisé dans l’optimisation des ressources en eau.
* **Valorisation des déchets et effluents de brasserie et de boulangerie :** pour permettre l’émergence de nouvelles voies de valorisation dans un secteur en plein boom (production de champignons, alimentation humaine, énergie, etc.)
* **Développement et renforcement des offres de couches lavables :** l'idée ici est de muscler les activités existantes à ce sujet et de proposer un travail sur une filière
* **Recyclerie sports et loisirs :** pour répondre aux enjeux environnementaux actuels, mais aussi à des besoins très concrets d’équipement pour les sportifs locaux et les familles à moindre coût
* **Transformation des risques liés aux espèces invasives en opportunité :** Les introductions, volontaires ou non, d’espèces exotiques animales et végétales, prennent des proportions désastreuses. Une espèce exotique peut devenir envahissante quand elle s’installe ou se développe dans un écosystème en devenant une perturbation nuisible à la biodiversité du milieu qui l’accueille. Valoriser une espèce invasive d’un point de vue socio-économique constituerait un levier intéressant dans la régulation de sa population.

L’équipe de la Serre va maintenant réaliser des études d’opportunité pour examiner si ces idées répondent aux besoins du territoire et peuvent donner naissance à un projet.

Ces études et futurs projets seront ainsi présentés au prochain comité de pilotage en novembre. Puis, les porteurs de projet pourront se positionner sur ces idées à partir de janvier 2023 via un [appel à candidatures.](https://www.laserre.org/appel-a-candidature/ "Appel à candidatures")

La Serre à projets a donc fait le plein de belles idées pour la nouvelle promotion à venir !