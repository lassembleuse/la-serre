---
date: 
title: La Serre à projets invitée de Durablement Vôtre
picture: "/uploads/DurablementVotre.jpeg"
publishdate: 2020-02-11T22:00:00+01:00

---
La Serre à projets était l'invitée le 21 janvier dernier de l'émission [Durablement Vôtre](https://sites.google.com/view/durablementvotre/accueil), animée par Eric Mutschler, et diffusée dans une quinzaine de radios locales du Grand Est. L'occasion d'un dialogue riche et motivant avec Bérengère Aba-Perea et Emmanuel Paul, chargés du projet au sein de France Active Lorraine et Kèpos. A écouter pour mieux comprendre les opportunités offertes par la Serre à projets !

<iframe style="height:315px" width="560" height="315" src="https://www.youtube.com/embed/Q0-YgL_iecE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>