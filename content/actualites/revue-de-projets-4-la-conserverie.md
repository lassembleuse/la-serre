---
date: 
title: 'Revue de projets #4 : la Conserverie'
picture: "/uploads/fermiersdici.jpg"
publishdate: 2021-02-15T00:00:00+01:00

---
Franck Magot nous conte aujourd'hui l'histoire des [Fermiers d'ici](https://www.lesfermiersdici.com/ "Les Fermiers d'ici") : traiteur bio et Made in Lorraine. [Accompagné aujourd'hui par la Serre à projets](https://www.laserre.org/projets/2020/la-conserverie-locale/ "La conserverie"), il nous explique pourquoi comment il compte nous en mettre plein les papilles avec le développement de sa conserverie !

**Qui es-tu ?**

_Je suis Franck Magot, Ingénieur Agronome et petit fils de paysans du Lot. J'ai créé et gère le service traiteur : Les Fermiers d'ici, inspiré de la cuisine de mes parents et grands parents._

**Comment est né le projet de créer une conserverie ?**

_Notre service traiteur propose des plateaux repas zéro déchet. Pour cela nous travaillons avec des bocaux que nous réutilisons. C'est ainsi qu'est née notre envie de créer une conserverie, pour mettre en bocal notre savoir faire de traiteur._

**En quoi consistera la conserverie ?**

_Nous souhaitons, grâce à cette conserverie, travailler avec les coopératives d'agriculteurs du Grand est, engagés en agriculture biologique. Grâce à ces producteurs, nous souhaitons proposer de succulents plats préparés aux senteurs de notre terroir. Nous proposerons des mets flexitariens, végétariens et vegans. Nous proposerons aussi des produits complémentaires : légumes tartinables, conserves de légumes...._

**Où en es-tu dans le projet ?**

_À ce jour, nous produisons et commercialisons 5 recettes : tajine d'agneau, sauté de veaux aux pleurotes, confits de porc à la sauce tomate, risotto au quinoa et pleurotes, curry végétarien. Une sixième recette sera proposée au mois de mars : tajine de légumes. Vous pouvez retrouvez ces recettes dans des magasins bio de Nancy (Biocoop, L'eau Vive, Carrefour Bio, Croc us, La chouette, L'épicerie du goût), des boulangeries nancéienne (Boulangerie Kohut, Lalonde Nathalie). Un magasin bio à Golbey : Univers'bio. Une épicerie (L'épicerie bio du quai) et un hotel (hotel b n b) à Metz._

**Qu’est-ce qui t'attends dans les prochains mois ?**

_Nous sommes entrain de questionner nos potentiels futurs clients afin de connaître leur besoin. Cela nous permettra d'établir un prévisionnel d'activité. Grâce à cette information nous pourrons dimensionner notre futur outil._

_Par ailleurs, nous rencontrons chaque mois de nombreux commerces. Le but est d'être implanté dans les magasins bio, boulangeries, boucheries en lorraine fin 2021. De ce fait, nous sommes à la recherche d'un chef cuisinier et de cuisiniers._

_Puis nous souhaitons remettre en route notre food truck, afin de proposer ces bocaux aux salariés, dans le Grand Nancy. Ainsi, nous aurons besoin de recruter un serveur pour assurer ce service._

_Enfin, nous souhaitons créer de nouvelles recettes, notamment végétariennes._

**Quel est l’avantage de mener cette initiative dans le cadre de la Serre à projets ?**

_Grâce à la serre à projets, nous sommes accompagnés dans notre développement. Ainsi, grâce à une réunion mensuelle, nous prenons de la hauteur sur notre projet et pouvons ainsi mieux planifier les futures étapes. Par ailleurs, nous présentons notre projet à divers partenaires : Région Grand Est, Département de Meurthe et Moselle, Metropole du Grand Nancy, ENSAIA, Agria, Scalen, Crédit Agricole. Ces réunions tous les 4 mois, nous permettent de confronter notre projet à de potentiels partenaires/ clients. Ces derniers ont aussi des informations qui nous permettent d'améliorer notre business model._