---
date: 
title: 'Revue de projets #9 : Les Coursiers Nancéiens'
picture: "/uploads/p1020428.JPG"
publishdate: 2022-06-27T13:00:00+02:00

---
Partons aujourd'hui à la rencontre de Mehdi Davouse, un féru de vélo à l'origine des [Coursiers Nancéiens](https://www.lescoursiersnanceiens.fr/ "https://www.lescoursiersnanceiens.fr/"). Ce jeune cycliste nous en dit plus sur ce projet de [cyclo-logistique accompagné par la Serre à projets depuis Avril 2021.](https://www.laserre.org/projets/2021/le-service-de-livraison-a-velo/ "https://www.laserre.org/projets/2021/le-service-de-livraison-a-velo/")

**Qui es-tu ?**

Je suis Mehdi Davouse, porteur du projet Les Coursiers Nancéiens. J’ai 35 ans. Nancéien de naissance et de cœur, fou amoureux de cyclisme et passionné par la cyclo-logistique. Je roule à vélo tous les jours, par loisir, pour m’entraîner, pour me déplacer mais aussi, et beaucoup, pour travailler. En effet, je suis « coursier » depuis maintenant cinq ans. J’ai commencé en tant que micro-entrepreneur sur  
mon temps libre après mes heures de travail et j’ai, par la suite, décidé d’en faire mon métier. On dit que je suis livreur à vélo ou coursier mais la définition exacte de mon activité est cyclo-logisticien.

**Comment est né le projet de créer une activité de cyclo-logistique ?**

C’est la combinaison de trois facteurs. Premièrement, j’ai travaillé il y a quelques années pour un transporteur logistique et en m’y intéressant j’ai été interpellé par le niveau de pollution émis par les acteurs de ce secteur, particulièrement avec la livraison du dernier kilomètre et l’explosion du @-commerce. Ensuite j’ai travaillé pendant trois ans pour deux plateformes Foodtech mais rapidement le modèle économique et social qu’elles proposaient/imposaient ne me convenait plus. Et enfin la crise sanitaire liée au Covid-19. Cela n’a pas déclenché les choses mais ça les a accélérées grandement (volume et diversités des livraisons et aménagements cyclables). Pour moi ce projet allait de soi car il était une réponse à plusieurs problématiques.

**Où en êtes-vous dans le projet ?**

Après s’être formé en association en Novembre 2020 nous allons très prochainement passer en SCOP. Nous diversifions notre offre de service en proposant de la cyclo-logistique, de la foodtech mais aussi du service à la personne.

**Qu’est-ce qui vous attend dans les prochains mois ?**

Un local est à venir, qui sera notre siège social mais aussi un hub qui servira aussi bien de PLU (Point de livraison urbain) que de stockage en zone tampon. Ce lieu aura aussi une utilité sociale via l’association que nous maintiendrons dans ce but. Une flotte de vélos cargos diversifiée et du matériel logistique afin de pouvoir s’adapter et répondre toujours plus efficacement à toute demande. Et surtout beaucoup de livraisons !

**Quel est l’avantage de mener cette initiative dans le cadre de la Serre à projets ?**

Les avantages sont multiples. Lorsque l’on porte un projet on a la tête dans le guidon si je puis dire. On en connait la finalité mais pas forcément (voire pas du tout !) les étapes pour y accéder. La Serre à projets est là pour nous accompagner, nous former à l’entrepreneuriat, nous permettre de confronter notre projet, de bénéficier d’un suivi personnalisé et bienveillant. La Serre permet également de faire des rencontres, notamment avec des acteurs de l’ESS, des porteurs de projets, des partenaires qui font de cet accompagnement une véritable aventure humaine.