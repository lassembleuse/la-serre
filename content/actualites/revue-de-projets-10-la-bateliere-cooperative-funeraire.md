---
date: 
title: 'Revue de projets #10 : La Batelière - Coopérative Funéraire'
picture: "/uploads/photo-article.JPG"
publishdate: 2022-11-28T13:00:00+01:00

---
Aujourd'hui, nous allons vous présenter un projet un peu particulier : [une coopérative funéraire](https://www.laserre.org/projets/2021/la-cooperative-funeraire/ "Coopérative funéraire") ! Mais, en quoi cela consiste ? Aurélie nous en dit plus dans cet article.

**Qui es-tu ?**

Bonjour, je m’appelle Aurélie DIDIER-LAURENT, vosgienne de 40 ans

**Comment est né le projet de créer une coopérative funéraire ?**

Je fais partie des membres fondatrices de l’association LE JOUR D’APRÈS, centre de ressources et d’accompagnement au deuil, créé en 2019.

Au sein de notre association, nous recevons de nombreuses familles endeuillées. Si certaines ont bien vécu le temps des obsèques de leur proche car elles ont été bien accompagnées, d’autres nous ont témoigné d’un manque d’accompagnement et de transparence des services funéraires. 

En tant qu’accompagnants au deuil, nous avons conscience que les jours qui entourent le décès d’un proche ont un impact sur le démarrage du processus de deuil. Les opérateurs funéraires ont donc un rôle fondamental et il est important qu’ils assument cette mission de service public en répondant, au plus près, aux besoins et préoccupations des familles. 

Or, certains besoins ne sont malheureusement pas toujours bien assouvis : besoin de connaître le droit funéraire (ce qui est obligatoire et ce qui ne l’est pas), besoin de temps pour prendre des décisions, besoin de sobriété écologique et besoin de transparence sur les tarifs. 

Petit à petit, nous avons donc sondé les citoyens et compris qu’il y avait un enjeu à faire en sorte que ceux-ci se réapproprient le temps des obsèques.

**En quoi consistera La Batelière ?**

La Batelière sera une pompe funèbre qui offrira un service funéraire à qui que ce soit, dans le respect du droit funéraire. 

Le fait de choisir le statut coopératif nous permettra de mettre toute notre énergie sur l’accompagnement global des familles plutôt que sur la vente de cercueils à des prix exorbitants et qui viennent de loin. Nous proposerons des temps de sensibilisation et d’information sur le droit funéraire. 

La Batelière privilégiera des produits éco-responsables et sera gérée en gouvernance partagée (familles, citoyens soutiens, salariés, membres engagés, partenaires, fournisseurs) À long terme, nous espérons travailler avec des fournisseurs 100% lorrains (cercueils, capitons, urnes), inscrits dans une démarche écologique, notamment à partir de matériaux de réemploi par exemple.

**Où en êtes-vous dans le projet ?**

Nous lançons l’appel à souscription. Qui souhaite soutenir le projet en prenant des parts sociales (30€ la part) est le bienvenu ! 

En parallèle, nous sommes à la recherche d’un local car nous envisageons une ouverture au premier trimestre 2023.

**Qu’est-ce qui vous attend dans les prochains mois ?**

Je suis actuellement en train de suivre la formation de dirigeant funéraire.

Nous étions présents au Village des Solutions au conseil Départemental les 4 et 5 novembre et espérons obtenir le plus de voix possible pour obtenir Les trophées de l’Encouragement.

Nous allons passer beaucoup de temps à rencontrer les citoyens pour présenter ce projet innovant et inviter à nous rejoindre. Des soirées vont être organisées ainsi que des interviews. Notre site internet est en construction et nous l’alimenterons notamment. En attendant, nous avons une page Facebook : [La Batelière - coopérative funéraire NANCY](https://www.facebook.com/profile.php?id=100086319762355 "Page FB Batelière").

L’AG constitutive de la SCIC (Société Collective d’Intérêt Collectif) devrait avoir lieu avant la fin 2022.

Je suis actuellement en train de suivre la formation de dirigeant funéraire.

**Quel est l’avantage de mener cette initiative dans le cadre de la Serre à projets** ?

Être accompagné par La Serre à Projets, c’est bénéficier de formations à la création d’entreprise de l’ESS. C’est également s’ouvrir aux questions de transition écologique et donc trouver la meilleure réponse, adaptée à notre domaine professionnel. 

Enfin, ce dispositif nous offre une mise en réseau incroyable avec énormément d’acteurs différents, et donc, de futurs fournisseurs et partenaires. Ce soutien nous offre une belle reconnaissance et permet de nous aider à casser ce tabou qui demeure autour de la mort, dans notre société. 

MERCI à LA SERRE À PROJETS !!