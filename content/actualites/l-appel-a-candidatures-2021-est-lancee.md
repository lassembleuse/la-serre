---
date: 
title: L'appel à candidatures 2021 est lancée
picture: "/uploads/illustration-article.png"
publishdate: 2021-01-14T08:00:00+01:00

---
[L’appel à candidatures](https://www.laserre.org/appel-a-candidature/) de la Serre à projets est lancé ! Il vous y est possible d'y répondre jusqu’au 28 février 2020.

Cette année, ce sont **6 thématiques** qui ont été sélectionnées par le Comité de pilotage de la Serre à projets et qui ont donné lieu à 6 études d’opportunité auxquels les candidats peuvent postuler :

1. [**Un bureau d’études Low-tech**](https://www.laserre.org/uploads/etude-bureau-d-etude-low-tech.pdf)
2. [**Un service de verdissement de la Ville**](https://www.laserre.org/uploads/etude-ville-verte.pdf)
3. [**Une vélo-école**](https://www.laserre.org/uploads/etude-velo-ecole.pdf)
4. [**Une galerie durable**](https://www.laserre.org/uploads/etude-galerie-durable.pdf)
5. [**Un service de cyclologistique**](https://www.laserre.org/uploads/etude-cyclologistique.pdf)
6. [**Un service de consigne du verre.**](https://www.laserre.org/uploads/etude-consigne-de-verre.pdf)

Il est important pour les personnes qui souhaitent postuler à l’appel à candidature de lire les études d’opportunité qui ont été réalisées. 

Vous pouvez retrouvez l’ensemble des projets de la promotion 2021 [**ici**](https://www.laserre.org/projets/2021/) ! À noter que les variantes sont autorisées.

Comme l’année dernière, les candidats auront également la possibilité de proposer leur propre projet et de se positionner sur le **volet blanc.**

Retrouvez toutes les informations concernant l'appel à candidatures [ici](https://www.laserre.org/appel-a-candidature/ "Appel à candidature") !