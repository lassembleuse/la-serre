---
date: 
title: 'Revue de projets #5 : Mimésis'
picture: "/uploads/jack-in-the-shadow.jpg"
publishdate: 2021-03-09T00:00:00+01:00

---
Mortimer Dubois est à la tête de [Mimésis](https://www.mimesis.bio/ "Mimésis site") , [un projet de composterie accompagné au sein de la Serre à projets.](https://www.laserre.org/projets/2020/collecte-et-valorisation-des-dechets-organiques/ "projet composterie") De quoi s'agit-il exactement ? Mortimer nous en dévoile un peu plus !

**Qui es-tu ?**

Je suis Mortimer, un citoyen engagé pour la sauvegarde de notre environnement et de sa biodiversité.

**Comment est né le projet de créer Mimésis ?**

Je cherchais à étendre mon mode de vie quotidien à ma sphère professionnelle et j'ai souhaité m'inscrire dans un projet qui ait du sens. J'aime faire du vélo, et j’ai la forte volonté de réduire les biodéchets qui partent pour l'incinération.

**En quoi consistera la composterie Mimésis ?**

Le principe de [Mimésis](https://www.mimesis.bio/ "Site Mimésis") est de détourner les biodéchets des incinérateurs pour éviter de brûler de l'eau. Nous collectons directement chez le producteur le plus souvent possible pour lui faciliter la tâche et éviter toutes sources de nuisance. Les biodéchets sont ensuite compostés pour devenir un engrais de qualité puisque les intrants sont variés et apportent des nutriments qui aident les plantes à mieux pousser. S’en suit la vente du compost pour enrichir le sol en lui redonnant de la vie, en dépolluant l'atmosphère par la même occasion : hop, d'une pierre deux coups !

**Où en es-tu dans le projet ?**

Je vais probablement bientôt pouvoir m’implanter sur un morceau de terrain. C'est en cours de discussion mais c'est en bonne voie. Côté composteurs, je suis en train de les faire designer sur mesure par des menuisiers de la région (Mimésis utilise un système de compostage innovant). Ceux-ci travaillent avec du bois local issu des forêts durables. Concernant le vélo, je suis en train de faire des formations et des entretiens personnalisés avec « Les Boites à vélo » qui ont créé "Ma cyclo entreprise" afin d’aider et accompagner les entrepreneurs à vélos. Cela me permet d'anticiper et d’y voir plus clair dans mon projet. Pour le financement, je travaille actuellement sur un projet de crowdfunding qui me permettra de payer le vélo et les composteurs, entre autres.

**Qu’est-ce qui t'attends dans les prochains mois ?**

Le concrétisation de ce dans quoi je m’investis actuellement, c’est à dire la gestion du terrain, l’acquisition du vélo et des composteurs et leur financement. Par la suite, il y aura le démarchage des clients et le commencement de l'activité, avec entre temps la création de la structure juridique.

**Quel est l’avantage de mener cette initiative dans le cadre de la Serre à projets ?**

Un grand avantage est d'avoir un rendez-vous mensuel qui me permet de faire un bilan intermédiaire de l’avancement de mon projet, soit de bénéficier d’un suivi solide. Cela m'aide à me recentrer et à me concentrer sur les priorités. Le fait d'avoir des personnes qui ont de l'expérience dans le domaine de la création d'entreprises est un réel atout. Sans eux, je n'aurais pas su par quoi commencer. La Serre à projets t'apporte un regard extérieur pour t'aider à prendre du recul et dans le même temps, ils t'accompagnent dans la progression des démarches que tu entreprends.