---
date: 
title: 'Retour sur le temps d''échange de la Serre à projets #2021'
picture: "/uploads/capture-d-ecran-temps-d-echange.png"
publishdate: 2021-02-18T10:00:00+01:00

---
[**Un temps d'échange et d'information**](https://www.laserre.org/actualites/temps-d-echange-et-d-information-sur-l-appel-a-candidatures/ "Temps d'échange") **de la Serre à projets a eu lieu le Mercredi 17 février en visioconférence.** 

Cet événement a réuni **une trentaine** de candidats potentiels, partenaires et curieux. Il était organisé en deux temps : 

* un premier pour présenter le dispositif de la Serre à projets et l'[appel à candidatures](https://www.laserre.org/appel-a-candidature/ "Appel à candidatures")
* un second pour réunir les participants en groupe de travail selon les [thématiques visées](https://www.laserre.org/projets/2021/ "6 thématiques retenues") dans le but de créer des synergies.

C'était également l'occasion de répondre aux questions des participants qui ont jusqu'au **28 février 2021 minuit** pour déposer leur candidature !

**Vous n'avez pas pu assister à cet événement ? Vous hésitez encore ? Vous avez des questions ?**

Il est encore temps ! [Contactez-nous !](https://www.laserre.org/contact/ "Contactez-nous !")