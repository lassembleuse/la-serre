---
date: 
title: 'Revue de projets #3 : Frugali'
picture: "/uploads/sans-titre.png"
publishdate: 2020-10-28T14:16:00+01:00

---
Anaïs, Chloé et Lucie ont toutes trois lancés [Frugali](https://www.facebook.com/frugali.fr/ "Frugali") : un cabinet de conseil et de formation en pratiques durables, consommation responsable et transition alimentaire. [Un projet accompagné fièrement au sein de la Serre à projets](https://www.laserre.org/projets/2020/un-dispositif-de-sensibilisation-a-la-cuisine-responsable/ "Serre"). Ces 3 entrepreneuses nous en dévoilent un peu plus !

**Qui êtes-vous ?**

_Anaïs Streit, cheffe de projet et stratégie ; Chloé Lelarge, formatrice et consultante et Lucie Devoille, designeuse et consultante ; toutes trois co-fondatrices du cabinet de conseil, de formation et de sensibilisation Frugali. Nos parcours et formations sont complémentaires, nous partageons le désir de faire évoluer les modes de consommation._

**Comment est né le projet de créer un cabinet de conseil et de formation en pratiques durables, consommation responsable et transition alimentaire ?**

_Nous nous croisions au détour de formations, de réunions Kèpos. La décision de nous retrouver toutes les trois autour d’un atelier anti-gaspillage de fabrication de confiture de bananes à la Conserverie Locale de Metz a marqué le début de cette aventure. Ce moment de partage, d’échanges a permis de révéler une synergie évidente. C’est à partir de ce moment que nous avons décidé de nous regrouper pour créer Frugali. Notre complémentarité ainsi que notre confiance mutuelle nous ont permis de construire des bases solides pour débuter le projet._

**En quoi consistera Frugali ?**

_Mêlant médiation et design, Frugali réinvente les modes de pensée autour de la consommation. Ce cabinet de conseil de formation et de sensibilisation a pour objectifs de :_

* _Transmettre et accompagner vers une nouvelle culture de consommation_
* _Eveiller les consom’acteurs à travers l'acquisition de pratiques professionnelles durables_
* _Sensibiliser les publics sur leur positionnement de consom’acteur_
* _Conseiller les entreprises sur des initiatives durables et responsables_
* _Acquérir des informations, les transmettre et les partager_
* _Jouer un rôle de centralisation, de coordination pour permettre de faire remonter les besoins émanant des acteurs du territoire auprès des décideurs politiques_

_L’offre de Frugali a été construite de manière à répondre à ces ambitions. Nos cœurs de métiers sont :_

* _La construction et l’animation de formations professionnelles initiales et continues_
* _La construction et l’animation d’ateliers et de conférences_
* _L’apport d’un service de conseils et d’accompagnements, le pilotage d’études Ad Hoc_

**Notre approche**

_Frugali a pour ambition de :_

* _Favoriser l'apprentissage par le plaisir et l'expérience, faire appel à la créativité_
* _Inspirer le changement et l'innovation positive_
* _Développer l'intelligence collective et soutenir la cohésion des équipes_

_Pour cela nos outils mêlent création et médiation, ils sont aux confins des arts plastiques, des échanges et de la verbalisation._

**Où en êtes-vous du projet ?**

_Nous avons réalisé une étude de faisabilité. Elle nous a permis de valider la nécessité de développer l'offre et de créer un réseau de partenaires._

_Cette étape a donné de la notoriété aux prémices de Frugali. Elle a validé le besoin auprès de clients potentiels. Parallèlement à cette étape de rédaction, le projet a débuté. Nous avons, à ce jour réalisé les premières prestations de formations, d’ateliers et de conférences._

_Aussi les supports de communication ont été construits, permettant une présence sur les réseaux sociaux. Le site internet est actuellement en cours de construction._

_Des ​ ateliers de sensibilisation à destination de collégiens ont été dispensé. A l’occasion de cette prestation Frugali a mis à l’honneur le partage et l’échange grâce à l’utilisation du théâtre forum et l’animation d’ateliers pratiques. L’ensemble des outils utilisés ont permis aux élèves de s’exprimer, de collaborer au service de la recherche de solutions._

_La fromagerie “Ma jolie crèmerie” de Bar le Duc nous a également fait confiance pour mettre en place un atelier sur le zéro déchet et le fromage._

_De belles perspectives sont prévues pour 2021._

**Qu’est-ce qui vous attend dans les prochains mois ?**

_Sur les mois de novembre, décembre et janvier un cycle de formations professionnelles sur la thématique de l'alimentation durable sera dispensé aux travailleurs sociaux bénévoles et bénéficiaires du département de la Meurthe-et-Moselle._

**Quel est l’avantage de mener cette initiative dans le cadre de la Serre à projets ?**

_Le soutien de la ​ Serre à projets ​ a été précieux dans la création de Frugali._

_L’accompagnement de 6 mois, les nombreux conseils et les formations ont contribué à la structuration ainsi qu’a l’ancrage du projet. La ​ Serre à projets nous guide dans les dernières démarches juridiques afin de faire évoluer Frugali vers un modèle d’entreprise relevant de l’économie sociale et solidaire._

_Enfin, la ​ Serre à projets permet à Frugali d’appartenir à un réseau d’acteurs engagés en matière de transition écologique. La mutualisation des moyens, des compétences mais aussi l’échange de bonnes pratiques et d’informations sont le meilleur allié._