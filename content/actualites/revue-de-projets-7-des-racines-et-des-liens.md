---
title: 'Revue de projets #7 : des Racines et des Liens'
subtitle: ''
sitemap:
  priority: 
picture: "/uploads/des-racines-et-des-liens.jpg"
publishdate: 2021-11-05T00:00:00+01:00
date: 

---
_Enzo Arcidiacono et Guell Théo, gérants_ [_Des Racines & Des Liens_](https://www.desracinesetdesliens.fr/ "Des Racines et des Liens")_._

_Conception, création et suivi de jardins nourriciers de manière participative et pédagogique._

**Comment est né le projet d'aménagement des friches & délaissé urbains ?**

Ce projet est né d'un constat commun, avec différentes entreprises et associations partenaires engagées dans la transition écologique, que l’engagement des citoyens est de plus en plus fort pour aménager de manière écologique et collective leurs environnements.

**En quoi consistera la proposition d'aménagement participatif et écologique des friches urbaines ?**

Nancy et sa métropole regorgent de friches urbaines et de délaissés urbains, depuis longtemps laissés à l’abandon. En fonction de leurs potentiels écologiques, sociaux et/ou attractifs, ces lieux peuvent rapidement devenir des lieux de vie dynamiques, productifs, créateurs de lien social et riches en biodiversité.

Dans l’hypothèse où les friches n’ont pas été polluées par le passé, elles peuvent être transformées en îlot de jardinage collectif. Dans le cas contraire, ces lieux peuvent être réaménagés en îlot de biodiversité (refuge pour la faune et la flore en milieu urbain) ou en îlot de fraîcheur. Si la friche est détectée comme un réservoir de biodiversité en milieu urbain, il est important de mettre en place une communication et des actions de sensibilisation à destination des habitants du quartier.

Toutes les étapes de réalisation de ces îlots doivent se faire de manière participative et pédagogique en lien avec les futurs usagers (concertations avec les habitants et élus - conception et aménagement des espaces et jardins - suivi et entretien, leviers de création de lien social).

**Où en êtes-vous dans le projet ?**

Nous avons rencontré l'ensemble des acteurs engagés dans l'aménagement actuel des friches urbaines et sommes au fait des différentes étapes clefs dans l'aménagement des friches et des délaissées urbains. Nous sommes en train d'élaborer le catalogue de prestations des différents îlots.

**Qu’est-ce qui vous attend dans les prochains mois ?**

Depuis début septembre, nous avons commencé l'aménagement d'une ancienne friche urbaine de 2000m2 sur la ville de Vandœuvre-lès-Nancy, après concertation et conception avec les habitants du quartier. L’étape suivante est la réalisation de l’îlot de biodiversité !

**Qu’est-ce que la Serre à projets vous a apporté ?**

La Serre à projet nous a permis de rencontrer de nombreux acteurs engagés dans la transition écologique et sociale avec la découverte de projets ambitieux et passionnants.

Aussi, nous sommes montés en compétences sur des domaines variés grâce à de nombreuses formations.

Enfin, nous bénéficions d'un accompagnement dans le suivi de la mise en place de notre projet, ce qui nous a permis d’avancer plus sereinement !