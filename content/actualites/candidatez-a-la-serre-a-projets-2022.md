---
date: 
title: 'Candidatez à la Serre à projets #2022 !'
picture: "/uploads/visuel_web_laserre.jpg"
publishdate: 2022-01-05T09:00:00+01:00

---
#### C'est parti pour le lancement de [**l’appel à candidatures**](https://www.laserre.org/appel-a-candidature/ "https://www.laserre.org/appel-a-candidature/") de la Serre à projets #2022 ! Vous avez jusqu'au **09 février 2022** pour postuler !!!

Cette année encore, l'équipe propose aux futurs entrepreneurs **6 belles thématiques** qui gravitent autour de la transition écologique et solidaire :

1. [**Une compagnie de vélo-bus et/ou vélo-taxi**](https://www.laserre.org/uploads/laserre_etude_opportinite_velobus_taxi.pdf "Vélo-bus et taxi")
2. [**Une unité de lavage du linge écoresponsable**](https://www.laserre.org/uploads/laserre_etude_opportinite_linge.pdf "Unité de lavage du linge")
3. [**Une** **activité de valorisation des coproduits et déchets brassicoles**](https://www.laserre.org/uploads/laserre_etude_opportinite_brassicole.pdf "Coproduits brassicoles")
4. [**Une** **activité de collecte et de traitement des biodéchets**](https://www.laserre.org/uploads/laserre_etude_opportinite_biodechets.pdf "Biodéchets")
5. [**Une activité de production et/ou de vente de fleurs coupées locales**](https://www.laserre.org/uploads/laserre_etude_opportinite_fleurs.pdf "Fleurs coupées locales")
6. [**Une cantine solidaire et écoresponsable**](https://app.forestry.io/sites/okrjxfv5jebkea/body-media//uploads/laserre_etude_opportinite_cantine_solidaire_vandoeuvre.pdf "/uploads/laserre_etude_opportinite_cantine_solidaire_vandoeuvre.pdf")

> #### La Serre à projets lance également [**Quartiers en transition**]() !
>
> L’objectif est d’accompagner la création d’activités répondant aux besoins spécifiques de deux quartiers sélectionnés : **« Les Provinces » à Laxou et « Les Nations » à Vandœuvre.**
>
> Voici les thématiques qui ont été sélectionnées spécifiquement pour ces quartiers :
>
> ##### \[Les Provinces - Laxou\] :
>
> * [**Une recyclerie “Jouets et enfance”**](https://www.laserre.org/uploads/laserre_etude_opportinite_ressourcerie_laxou.pdf "/uploads/laserre_etude_opportinite_ressourcerie_laxou.pdf")
> * [**Un tiers-lieu de la transition écologique**](https://www.laserre.org/uploads/laserre_etude_opportinite_tierslieu_laxou.pdf "/uploads/laserre_etude_opportinite_tierslieu_laxou.pdf")
>
> ##### \[Les Nations - Vandœuvre\] :
>
> * [**Une activité de cuisine partagée**](https://www.laserre.org/uploads/laserre_etude_opportinite_cuisine_partagee_vandoeuvre.pdf "/uploads/laserre_etude_opportinite_cuisine_partagee_vandoeuvre.pdf")
> * À noter que dans ce quartier, il sera également possible de développer le projet de [**cantine solidaire**.](https://www.laserre.org/uploads/laserre_etude_opportinite_cantine_solidaire_vandoeuvre.pdf "/uploads/laserre_etude_opportinite_cantine_solidaire_vandoeuvre.pdf")

Vous pouvez retrouvez l’ensemble des projets de la promotion 2022 [**ici**](https://www.laserre.org/projets/2022/) !

Comme l’année dernière, les candidats auront également la possibilité de proposer leur propre projet et de se positionner sur le **volet blanc.**

### **Retrouvez toutes les informations concernant l’appel à candidatures** [**ici**](https://www.laserre.org/appel-a-candidature/ "Appel à candidature") **!**