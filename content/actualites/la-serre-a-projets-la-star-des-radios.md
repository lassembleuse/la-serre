---
date: 
title: 'La Serre à projets : la star des radios !'
picture: "/uploads/microphone-1562354_1920.jpg"
publishdate: 2021-02-23T11:30:00+01:00

---
La Serre à projets a effectué un certain nombre de passage radios ces dernières semaines pour présenter l'[appel à candidatures de la Serre à projets](https://www.laserre.org/appel-a-candidature/ "Appel à candidatures").

En voici un récapitulatif :

* **RCF :** "Citoyens de demain" : [https://rcf.fr/embed/2580979](https://rcf.fr/embed/2580979 "https://rcf.fr/embed/2580979") et "Le grand invité" : [https://rcf.fr/embed/2574041](https://rcf.fr/embed/2574041)
* **France Bleu :** [https://www.francebleu.fr/infos/economie-social/la-nouvelle-eco-dans-le-grand-nancy-la-serre-a-projets-cherche-les-bonnes-idees-1613411278](https://www.francebleu.fr/infos/economie-social/la-nouvelle-eco-dans-le-grand-nancy-la-serre-a-projets-cherche-les-bonnes-idees-1613411278)
* **RCN :** [http://www.rcn-radio.org/index.php/album/bio-divers-cite/](http://www.rcn-radio.org/index.php/album/bio-divers-cite/) (émission du 10 février 2021 à partir de 7min13)
* **Fajet :** [https://www.mixcloud.com/fran%C3%A7ois-barthelemy/petit-retour-sur-linterview-du-jour-la-serre-%C3%A0-porjet-avec-ian-mclaughlin/?fbclid=IwAR3qQQNHaEb-iwabIcTpTU40QH8JQGbbJ5QhPAr2of3dsHsvz1cIAeHcIIY](https://www.mixcloud.com/fran%C3%A7ois-barthelemy/petit-retour-sur-linterview-du-jour-la-serre-%C3%A0-porjet-avec-ian-mclaughlin/?fbclid=IwAR3qQQNHaEb-iwabIcTpTU40QH8JQGbbJ5QhPAr2of3dsHsvz1cIAeHcIIY)

Bonne écoute !

[L'appel à candidatures court jusqu'au 28 février 2021](https://www.laserre.org/appel-a-candidature/ "Appel à candidatures"). **Il n'est pas trop tard pour déposer un dossier !**

Des questions ? [**Contactez-nous !**](https://www.laserre.org/contact/ "Contactez-nous")