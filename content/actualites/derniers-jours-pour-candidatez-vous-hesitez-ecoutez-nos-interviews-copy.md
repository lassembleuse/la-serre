---
date: 
title: On parle de l'appel à candidatures sur les radios locales !
picture: "/uploads/radio-ge326369ce_1920.jpg"
publishdate: 2023-02-14T11:00:00+01:00

---
Alors que l'**appel à candidatures touche bientôt à sa fin** ([vite, viiite, candidatez !](https://www.laserre.org/appel-a-candidature/ "Candidatez !")), nous faisons un point sur les interventions de La Serre à projets sur les différentes antennes radiophoniques locales !

En voici un récapitulatif :

* **RCN :** [https://lstu.fr/rcn](https://lstu.fr/rcn "https://lstu.fr/rcn")
* **Fajet :** [https://lstu.fr/fajet-2](https://lstu.fr/fajet-2 "https://lstu.fr/fajet-2")
* **RCF :** [https://lstu.fr/rcf](https://lstu.fr/rcf "https://lstu.fr/rcf")

Bonne écoute !

[L’appel à candidatures court jusqu’au 19 février 2022.]() **Il n’est pas trop tard pour déposer un dossier !**

Des questions ? [**Contactez-nous !**](https://www.laserre.org/contact/ "Contactez-nous")