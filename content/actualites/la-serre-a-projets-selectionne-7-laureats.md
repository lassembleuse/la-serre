---
date: 
title: La Serre à projets sélectionne 7 lauréats
picture: "/uploads/Photo Afterwork.jpg"
publishdate: 2020-04-22T22:00:00.000+00:00

---
[L'appel à candidatures]( "https://www.laserre.org/appel-a-candidature/") lancé par la Serre à projets s'est clos le 15 mars dernier, après la tenue d'un [afterwork le 13 février](), qui a mobilisé plus de 50 personnes. L'occasion pour les porteurs de projets d'échanger sur leurs projets respectifs, et de poser toutes leurs questions aux équipes de [France Active Lorraine](https://www.franceactive-grandest.org/lorraine/) et [Kèpos](https://www.kepos.fr/).

La Serre a Projets a obtenu 20 réponses de candidats fortement impliqués dans les diverses thématiques de la transition écologique et solidaire. Un beau succès pour cette première édition, avec des projets de très bonne qualité, et beaucoup d'idées prometteuses !

Son jury, réuni le mercredi 8 avril 2020, a sélectionné 7 projets dont la Serre à projets va accompagner la construction et le développement sur les prochains mois.

Ont donc été donc sélectionnés les projets suivants :

* Une cantine solidaire permettant de développer le lien social sur le Lunévillois, portée par [l’association Ecurie N°8](http://ecurie8.org/).
* Une action de sensibilisation à l’alimentation responsable portée par Chloé Lelarge.
* Une composterie pour valoriser des biodéchets avec un ramassage à vélo, avec Mortimer et Martin Dubois.
* Une trucothèque pour faciliter l’équipement des étudiants nancéiens, initiée par la [MJC Lorraine]().
* Une conserverie permettant la valorisation des produits agricoles bio et locaux par [les Fermiers d’Ici]().
* Une pépinière de quartier pour créer et entretenir le lien social, imaginée par Emilie Clavel.
* Une recyclerie pour la valorisation des meubles avec une inspiration Art Nouveau, nommée la Benne Idée !

L'accompagnement va maintenant débuter pour chacun de ces sept projets. Objectif : réaliser dans les six mois une étude de faisabilité complète, et bien sûr mobiliser des partenaires et aller chercher des financements.

Rendez-vous très bientôt sur ce site pour découvrir les portaits de chacun de ces porteurs de projets !