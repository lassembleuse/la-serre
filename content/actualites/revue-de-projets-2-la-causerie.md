---
date: 
title: 'Revue de projets #2 : la Causerie'
picture: "/uploads/emilie-clavel-2.jpg"
publishdate: 2020-09-29T00:00:00+02:00

---
Emilie Clavel [porte au sein de la Serre un projets de pépinière de quartier](https://www.laserre.org/projets/2020/pepiniere-de-quartier/), nommée [la Causerie](https://lacauserie54.wixsite.com/website1?fbclid=IwAR0JKckylXGsL0LF5QpidJRUCEi6Qb2F2JXEru6mnZ8bjfbZqRv9sIniLOQ). Mais de quoi s'agit-il ? Emilie nous en dit plus !

**Qui êtes-vous ?** 

_Emilie Clavel, porteuse du projet de l'association La Causerie, pépinière de quartier. Comptable de formation, j'ai eu l'occasion d'évoluer successivement chez Pôle Emploi puis chez un bailleur social. Mes valeurs m'ont guidée actuellement vers une reconversion professionnelle pour devenir maître composteur._

**Comment est né le projet de créer une pépinière de quartier?**

_Cela a été un processus de plusieurs années. D'abord, je suis passée par une prise de conscience sur nos modes de consommation, j'ai visité plusieurs tiers lieux parisiens et ai constaté que Nancy était aussi propice au développement de tels lieux. Puis des voisins m'ont proposé de me prêter un jardin ouvrier tout en me dispensant conseils et bienveillance. Déjà sensibilisés à la réduction des déchets, à la consommation de produits locaux et de saison, nous avons pu développer un projet familial afin de tester, observer, échanger, partager et essuyer des échecs sur cette parcelle au cœur de la ville. Au fil des discussions avec mon entourage, je me suis aperçue que beaucoup avait cette envie de reconnexion au jardin, mais la peur de l'échec, le manque de temps, d'espace, de connaissance ou de matériel empêchaient le passage à l'acte. En me renseignant, j'ai découvert des pionniers ailleurs en France et en Belgique, très inspirants, comme la_ [_Ferme du Chant des Cailles_](http://www.chantdescailles.be/)_, la_ [_Pépinière Tournai_](http://lapepiniere-tournai.be/) _ou encore_ [_Pépins Production_](https://www.pepinsproduction.fr/)_. C'est de cette façon qu'est né le projet. L'appel à porteur de projet de La Serre a été un déclencheur concret pour se lancer dans l'aventure!_

**En quoi consistera la Causerie ?**

_Créée en 2020, La Causerie s'est donnée pour ambition de promouvoir de manière populaire et accessible l'agriculture urbaine sur la Métropole du Grand Nancy, la biodiversité, le lien social autour du jardinage au naturel. L'association proposera notamment des activités citoyennes et intergénérationnelles en lien avec la sensibilisation à l'environnement au sens large, à la réduction des déchets, et de la transmission des savoirs tout en étant ouverte sur son quartier d'accueil, les Rives de Meurthe. La Causerie souhaite s'inscrire dans les politiques déjà mises en place par la_ [_Métropole_](https://www.grandnancy.eu/accueil/) _et la_ [_Ville_](https://www.nancy.fr/) _en faveur de la transition écologique, la relocalisation de la production et la végétalisation urbaine._

**Où en êtes-vous du projet ?**

_Au cours de l'étude de faisabilité, j'ai pu nouer de nombreux contacts très positifs avec d'autres associations nancéiennes et représentants d'institutions. Les retours sur le projet ont été si rapide que l'association s'est déjà vue proposer par le_ [_lycée Loritz_](http://www4.ac-nancy-metz.fr/lyc-loritz/)_, très engagé dans la transition écologique, l'installation de la première serre au sein de son établissement . Des partenariats sont également envisagés avec le Pôle senior du_ [_CCAS de Nancy_](https://www.nancy.fr/utile/social-et-solidarites/le-centre-communal-d-action-sociale-au-service-des-nanceiens-tout-au-long-de-leur-vie-337.html)_, avec_ [_ATP Rives de Meurthe_](https://rives-de-meurthe.fr/)_, et_ [_l'Autre Canal._](https://lautrecanalnancy.fr/) _En parallèle, je continue de me former sur le métier de maître composteur et sur celui de pépiniériste de quartier. Le collectif de l'association travaille à l'élaboration d'un projet pédagogique avec le lycée et à l'acrobatique plan de financement!_

**Qu'est-ce qui vous attend dans les prochains mois ?**

_Nous allons devoir trouver des ressources pour financer et installer une serre, faire grandir et connaître l'association en participant à des événements, établir un plan de culture, construire et animer des séquences pédagogiques, travailler en partenariat avec le lycée, renforcer les liens avec les institutions comme par exemple la Ville de Nancy et la Métropole._

**Quel est l'avantage de mener cette initiative dans le cadre de la Serre à projets ?**

_Plusieurs avantages se dessinent à être accompagnée par La Serre à projets. Tout d'abord, donner une impulsion concrète à une idée en gestation, avoir aussi accès à des formations spécifiques pour nous guider dans nos démarches et être encadrée par des référents et "parrains" de la Serre à projet. Ensuite ce dispositif permet de bénéficier "d'un carnet d'adresse" personnalisé en fonction de son projet, nous pouvons ainsi accéder aux interlocuteurs référents rapidement. Et enfin, l'accès à un espace de coworking va beaucoup aider les lauréats à créer des synergies entre les projets._

**Merci !**