---
date: 
title: 'La Serre à projets #2022 : de nouvelles idées sélectionnées et un déploiement
  dans les QPV !'
picture: "/uploads/210781656_2828378207476378_1540834674164035763_n.jpg"
publishdate: 2021-07-13T10:00:00+02:00

---
Le Comité de Pilotage de la Serre à projets s’est réuni le lundi 5 juillet 2021 à Nancy afin de procéder à la sélection des idées d’activités qui vont faire l’objet d’une **étude d’opportunité.**

Comme l'année dernière, la plupart des idées présentées au comité émergent de [Start Up de Territoire](https://www.facebook.com/startupterritoire54/ "startupterritoire54"). Il y en avait une vingtaine au total.

Après un vote et de nombreux débats entre les participants, chacun partageant ses connaissances du territoire et ses expériences, ce sont 7 idées qui ont été finalement retenues :

* **Producteur fleuriste de fleurs locales :** l’idée est de favoriser l’installation d’un producteur-fleuriste ou de travailler avec les producteurs de fleurs existants pour proposer plus de fleurs locales.
* **Compagnie de vélo-bus et/ou vélo-taxis :** développement d’un service de transports en commun et/ou pour particuliers à vélo.
* **Cantine solidaire :** restaurant associatif qui a pour but de lutter contre le gaspillage alimentaire et de permettre l’accès à des repas équilibrés, même aux plus démunis. À noter que cette thématique avait déjà été retenue il y a 2 ans, mais que le comité a considéré qu'il était intéressant de la proposer à nouveau.
*  **Valorisation des déchets et effluents de brasserie :** pour permettre l’émergence de nouvelles voies de valorisation dans un secteur en plein boom (production de champignons, alimentation humaine, énergie, etc.)
* **Centrale de lavage du linge écoresponsable :** les machines à laver personnelles et laveries classiques actuelles sont très polluantes. Cette solution pourrait permettre également de faciliter l’usage de solutions textiles lavables (couches, essuie-tout, mouchoir, etc.).
* **Unité de production d'énergie locale et renouvelable :** pour permettre à un particulier, un collectif, une entreprise de produire sa propre énergie à partir des ressources locales (Solaire, éolien, biomasse).

L’équipe de la Serre va maintenant réaliser des études d’opportunité pour examiner si ces idées répondent aux besoins du territoire et peuvent donner naissance à un projet.

Ces études et futurs projets seront ainsi présentés au prochain comité de pilotage en novembre. Puis, les porteurs de projet pourront se positionner sur ces idées à partir de décembre 2021 via un [appel à candidatures.](https://www.laserre.org/appel-a-candidature/ "Appel à candidatures")

Suite à la sélection des idées, les porteurs de projets de la promotion actuelle ont eu la chance de présenter leur projet devant les membres du comité de pilotage. Ainsi, nous avons pu assister à la présentation de :

* [**Remise**](https://www.laserre.org/projets/2021/la-valorisation-du-reemploi-dans-le-batiment/ "Remise")**,** la recyclerie du bâtiment, présentée par Emilie Lemoine et Christelle Hopfner.
* [**Les Coursiers Nancéiens**]()**,** les coursiers à vélo, présenté par Mehdi Davouse. 
* [**Le bureau d'études Low-tech**]()**,** présenté par Stéphane Haessler.
* [**Des Racines & des Liens**](), projet engagé sur le thème de la ville verte, présenté par Enzo Arcidiacono et Théo Guell.
* [**La Coopérative funéraire**](https://www.laserre.org/projets/2021/la-cooperative-funeraire/ "Coop Funéraire"), présentée par Aurélie Didier-Laurent.
* [**La vélo-école**](https://www.laserre.org/projets/2021/la-velo-ecole/ "Vélo-école"), présentée par Hadrien Fournet.

### La Serre à projets se lance dans les QPV !

La Serre souhaite déployer ce dispositif plus spécifiquement sur les quartiers prioritaires de la politique de la Ville (QPV) de la Métropole du Grand Nancy. L’objectif est d’accompagner la création d’activités répondant aux besoins spécifiques de ces quartiers.

Dans un premier temps des entretiens téléphoniques ont été réalisés auprès des chefs de projets territoriaux de la Métropole du Grand Nancy afin d’évaluer les dynamiques existantes sur les différents QPV.

Puis les résultats de ces échanges ont été présentés auprès des services de l’Etat et de la Métropole du Grand Nancy le 1er juin 2021 afin de définir les deux QPV où une étude plus approfondie serait effectuée. **Le choix s’est porté sur « Les Provinces » à Laxou et « Les Nations » à Vandœuvre.**

Les thématiques retenues au sein de ces quartiers et qui vont être étudiés sont :

* Recyclerie/Repair café & cantine
* Recyclerie "Jouets et enfance"
* Dark kitchen et/ou incubateur culinaire

La Serre à projets a donc fait le plein de belles idées pour la nouvelle promotion à venir !