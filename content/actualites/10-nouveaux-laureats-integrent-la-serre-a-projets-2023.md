---
date: 
title: '10 nouveaux lauréats intégrent La Serre à projets #2023 !'
picture: "/uploads/img_7329.jpg"
publishdate: 2023-04-03T09:00:00+02:00

---
### [Découvrez les **nouveaux lauréats** pour la promotion #2023 !](https://www.laserre.org/projets/2023/ "Promo 2023")

Suite à [l’appel à candidatures](https://www.laserre.org/appel-a-candidature/ "appel à candidatures") lancé en janvier 2023, la Serre a Projets a obtenu **26 réponse**s de candidats fortement impliqués dans les diverses thématiques de la transition écologique et solidaire. Le jury de sélection s’est réuni le 2 mars 2023 et a sélectionné **10 projets** dont les porteurs bénéficieront d’un accompagnement personnalisé et de formations collectives, en partenariat avec les acteurs et réseaux existants et ce jusqu’à la création de leur activité.

La **4ème promotion de la Serre à projets** sera donc composée des projets suivants :

\- [**une recyclerie du sport et loisirs**](https://www.laserre.org/projets/2023/recyclerie-de-sport-loisirs-et-ou-jouets/ "recyclerie sport") pour permettre aux sportifs locaux et aux famille de s’équiper en matériel à moindre coût – _projet co-porté par Lorraine Sport Services et Envie 2E_

\- [**une recyclerie de jouets et de livre**](https://www.laserre.org/projets/2023/recyclerie-de-sport-loisirs-et-ou-jouets/ "Recyclerie livre") qui s’adressera aux particuliers, mais également aux professionnels de la petite enfance – _projet porté par Alexandra PIANESE_

\- une activité de [**transformation de la renouée du Japon**](https://www.laserre.org/projets/2023/valorisation-ou-lutte-contre-des-especes-invasives/ "Espèces invasives") en matériau de construction – _projet porté par Maude BAYLE_

\- un projet de [**valorisation chimique et énergétique de la renouée du Japon**]() à travers le renforcement d’une filière de valorisation des espèces invasives – _projet porté par Flavien ZANINI de la société PROFILIA_

\- une activité de [**garage rétrofit**](https://www.laserre.org/projets/2023/garage-retrofit/ "Garage rétrofit") qui a pour objectif de convertir des véhicules thermiques en véhicules électriques et permettre au plus grand nombre d’accéder à cette solution – _projet porté par Didier MULLER de la société MULLER VEHICULES PROFESSIONNELS_

\- une activité de [**confection de culottes menstruelles**](https://www.laserre.org/projets/2023/developpement-ou-renforcement-de-l-offre-en-protection-et-couches-lavables/ "protection lavable") locales et respectueuses de l’environnement – _projet porté par Hélène CONNES de SUPER KWETSCH_

\- un projet qui de création d'une collection évolutive et adaptée aux [**couches lavables**](https://www.laserre.org/projets/2023/developpement-ou-renforcement-de-l-offre-en-protection-et-couches-lavables/ "couches lavables") ainsi qu’une culotte de protection 100% laine Mérinos française, qui a pour objectif de faciliter la motricité et le confort du bébé  – _projet porté par Sandra MARTINEZ de COCOTTE DE MAILLES_

\- une activité qui a pour objectif de promouvoir des [**comportements écoresponsables auprès des organisateurs d’événements**](https://www.laserre.org/projets/2023/manifestations-ecoresponsables/ "écomanifestation") – _projet porté par Victor TRAPP en collaboration avec Eco-Manifestations Réseau Grand Est_

\- création d’une communauté de [**valorisation des appareils médicaux**](https://www.laserre.org/projets/2023/filiere-de-reemploi-de-materiels-medicaux/ "recyclerie médical") qui a pour objectif la création d’une filière de reconditionnement – _projet porté par Adrien BAILLY de la société CARDIOLAB_

\- [**un pôle d’excellence du cuir**](https://www.laserre.org/projets/2023/pole-d-excellence-du-cuir/ "Pôle du cuir") imaginé par un collectif qui travaille au service du cuir et qui souhaite engager des démarches de coopération pour créer de l’emploi local et soutenir la filière. - _projet porté par Maude VANOBBERGHEN, Julien CHALON, Audrey GASC et Quentin SIMARD._