---
date: 
title: 'Revue de projets #6 : Lacagette'
picture: "/uploads/unnamed-1.jpg"
publishdate: 2021-06-29T00:00:00+02:00

---
Parmi les projets accompagnés par la Serre à projets en 2021 figure le collectif Lacagette et son projet d'[atelier de construction mobile et participatif](https://www.laserre.org/projets/2021/l-atelier-de-construction-mobile-et-participatif/ "atelier de construction mobile"). De quoi s'agit-il ? Ces débrouillards au grand cœur débordants d'imagination nous expliquent tout !

**Qui êtes-vous ?**

Nous représentons le collectif Lacagette, qui rassemble différentes personnes aux compétences complémentaires et multiples.

Parmi elles, un bureau de trois personnes : Nicolas, Elea et Laura.

Deux constructeurs fondateurs : Tanguy et Léo.

La construction est le cœur du collectif, c’est le point de départ.

Nos actions : La scénographie, gestion et organisation d’événements, chantiers participatifs, animations, régie d’exposition

**Comment est né le projet de créer un atelier de construction mobile et participatif ?**

Nous travaillons depuis plusieurs années sur la mise en place de moments de construction collectifs. Ces projets peuvent voir le jour grâce à des partenaires ambitieux d’initier ces actions. Ces opportunités sont devenues pour nous un domaine à creuser, à développer collectivement.

Pour cela nous voulons créer un dispositif complet qui permet l’installation d’un espace de construction temporaire. Nous imaginons un atelier entièrement mobile avec des éléments conçus pour être transportés et déplacés facilement.

**En quoi consistera l'atelier de construction mobile et participatif ?**

L’atelier de construction mobile est un outil à part entière, élaboré grâce aux principes de la démarche participative. Cet objet se veut mobile, accessible, polyvalent, adapté et partagé.

**_Plusieurs directions :_**

ATELIER PARTICIPATIF : Il se déroule sur une courte période (un ou plusieurs après midi), l’idée étant de proposer une initiation aux participants. Nous abordons les principes de bases de la construction et quelques notions pratiques. L’atelier participatif est destiné à tout public, mais avec une orientation plus ciblée pour le jeune public

CHANTIER PARTICIPATIF : Il se déroule sur une moyenne ou longue période. Le chantier participatif propose un approfondissement des bases de la construction, l’apprentissage du travail en collectif et la gestion globale d’un projet. L’objet du projet, les plans et les matériaux sont définis à l’avance

WORKSHOP : Un Workshop se déroule généralement sur une ou deux semaines, la thématique de projet est proposée, les plans et le concept ne sont pas définis à l’avance. Une phase de conception collaborative est amorcée pour donner suite à la phase de fabrication. La recherche d’expérimentation est une composante forte de l’idée Workshop.

Aussi, nous avons pour idée de créer des ateliers artistiques en lien avec un artiste et la mise en place de formations pour la pratique de l’atelier et du travail en collectif. Grâce au groupe de suivi mis en place avec la Serre à projets, il a également été abordé la question du réemploi de matériaux grâce aux démontages d’expositions.

**Où en êtes-vous dans le projet ?**

Nous travaillons avec un ensemble de partenaires pour continuer d’avancer dans cette démarche. Nous sommes en phase de recherches et d’analyses, avec pour envie de faire évoluer notre structure associative vers une forme coopérative.

Une étude technique et économique est en cours pour définir les futures étapes de ce projet.

**Qu’est-ce qui vous attend dans les prochains mois ?**

Avant même que le projet de l’atelier ne soit encore mis sur pied, nous continuons de répondre positivement à des propositions d’ateliers participatifs.

Nous allons travailler avec la Ville de Nancy pour la création de deux espaces «arbre à livres / lectures», un situé au Plateau de Haye et un autre à la Pépinière. Ces projets sont le fruit de la démarche du budget participatif initié par la ville.

Aussi nous sommes en concertation avec le Conseil Départemental de Meurthe et Moselle, le CAUE et l’association Prev’en Scene pour la mise en place d’un chantier participatif afin d’imaginer la scénographie de Jardin Extraordinaire 2021 début Octobre.

Aussi nous allons finaliser les différentes études en cours, afin d’engager les démarches nécessaires au montage du projet.

**Quel est l’avantage de mener cette initiative dans le cadre de la Serre à projets ?**

Ce n’est pas un avantage mais bien des avantages au pluriel !

La Serre est bien plus qu’un accompagnement. Au delà du soutien sur l’aspect technico-administratif, la Serre est un vrai réseau d’acteurs engagés, prêt à transmettre, partager, imaginer, initier et développer ces nouvelles envies et besoins.

C’est avec un état d’esprit positif, ancré si fortement, que se dégage toute la bienveillance autour de ce projet.