---
date: 
title: Temps d'échange et d'information sur l'appel à candidatures
picture: "/uploads/banniere-afterwork-17022021.png"
publishdate: 2021-01-28T00:00:00+01:00

---
[L’appel à candidatures](https://www.laserre.org/appel-a-candidature/) de la Serre à projets est maintenant lancé. Il court jusqu’au 28 février 2021.

Pour que l’équipe de la Serre à projets puisse répondre à toutes vos questions, et pour que les porteurs de projets puissent se rencontrer, rendez-vous donc au temps d'échange de la Serre à projets :

**Le mercredi 17 février à 17h30 en ligne**

Merci de vous y  inscrire en vous rendant sur le lien suivant : [https://framaforms.org/inscription-au-temps-dechange-de-la-serre-a-projets-1611571504](https://framaforms.org/inscription-au-temps-dechange-de-la-serre-a-projets-1611571504 "https://framaforms.org/inscription-au-temps-dechange-de-la-serre-a-projets-1611571504")

A bientôt !