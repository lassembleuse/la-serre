---
date: 
title: 'Revue de projets #1 : la Benne idée'
picture: "/uploads/chloe-et-antoine.jpg"
publishdate: 2020-07-15T15:30:00+02:00

---
Parmi les projets accompagnés par la Serre à projets en 2020 figure une recyclerie, nommée la Benne idée ! Retour avec ses promoteurs sur l'itinéraire de cette belle dynamique !

**Qui êtes-vous ?**

_Chloé Geiss et Antoine Plantier, porteurs du projet de recyclerie créative La Benne Idée. Nous sommes deux ingénieurs naturalistes de formation, agronome et géologue. Nos premières expériences professionnelles nous ont éloigné de nos idéaux, c’est pourquoi nous avons souhaité développer ce projet, non lucratif, à la croisée de l’économie circulaire et de l’économie sociale et solidaire._

**Comment est né le projet de créer une recyclerie ?**

_Ca a été un long processus. Cela faisait plusieurs années que l’on réfléchissait à un changement dans nos vies respectives. Et puis en 2019 nous nous sommes dit qu’il était temps. Après avoir visité un grand nombre de tiers lieux en Ile de France, nous nous sommes arrêtés sur l’idée de la recyclerie créative. Chloé souhaitait s’impliquer sur la problématique de la diminution des déchets, et du « faire soi même », Antoine avait une expérience dans le recyclage et souhaitait un aspect créatif. L’idée a fait son chemin. On l’a confirmée en visitant et en faisant du bénévolat dans d’autres recycleries. Depuis la fin 2019 nous développons ce projet sur le territoire du Grand Nancy._

**En quoi consistera la Benne Idée ?**

_Il s’agira d’une recyclerie créative de mobilier et articles de décoration. C'est-à-dire un lieu dans lequel nous ferons de la sensibilisation, de la collecte de « déchets » mobilier, de la valorisation, et de la vente. La valorisation ira du simple nettoyage à de la réparation et de la création. Des ateliers seront proposés pour apprendre à confectionner des objets ou du mobilier. Nous espérons aussi avoir un espace convivial type café. Les modèles que nous avons en tête sont_ [_La Collecterie_](https://lacollecterie.org/) _à Montreuil, et l’_[_Atelier D’éco Solidaire_](https://atelierdecosolidaire.com/) _à Bordeaux._

_Nancy a une riche histoire dans le secteur des arts décoratifs, avec l’Ecole de Nancy qui était le fer de lance de l’Art Nouveau. En quelques sortes nous souhaitons développer l’Art du Renouveau, en donnant une deuxième vie aux objets. L’Ecole de Nancy était engagée socialement et voulait développer un art dans tout et pour tous, inspiré et respectueux de la nature. Nous nous retrouvons dans ces valeurs._

**Où en êtes-vous du projet ?**

_Nous avons effectué une étude d’opportunité en rencontrant un grand nombre d’organismes publics et les acteurs locaux du réemploi. La Serre à Projets a effectué la même démarche et abouti à la même conclusion : le souhait de voir s’implanter une recyclerie sur le territoire du Grand Nancy. Depuis, nous réalisons une étude de faisabilité et sommes à mi-parcours de celle-ci. Le diagnostic territorial est établi. Nous entrons dans la seconde phase, plus périlleuse, de dimensionnement et modèle financier._

**Qu’est-ce qui vous attend dans les prochains mois ?**

_La création d’une recyclerie est un long processus, entre 2 et 5 ans selon l’Ademe. Nous allons finaliser l’étude de faisabilité d’ici la fin d’année. Plusieurs options seront ensuite envisageables, commencer l’activité avec une dimension réduite, ou chercher les financements nécessaires pour démarrer directement avec une taille critique. Nous espérons créer une dynamique citoyenne et structurer la gouvernance de l’association. Plusieurs éléments nous ont démontré qu’il y a une réelle volonté de voir émerger un projet de recyclerie. Ce projet est celui du territoire, ce n’est pas le nôtre, nous le portons simplement. Quoi qu’il en soit, nous aurons besoin d’un local, de la participation des citoyens, et du soutien des organismes publics._

**Quel est l’avantage de mener cette initiative dans le cadre de la Serre à projets ?**

_La Serre apporte de nombreux atouts pour la réussite d’un tel projet. Le principal est peut être la visibilité auprès des organismes publics, qui font partie du comité de pilotage. Cela permet de crédibiliser la démarche. Nous disposons également de précieux conseils pour les mises en relation et pour certains aspects entrepreneuriaux dont nous n’avions parfois pas conscience. Devant la complexité de la tâche cela nous donne aussi la motivation pour avancer en respectant certains délais._

**Merci !**