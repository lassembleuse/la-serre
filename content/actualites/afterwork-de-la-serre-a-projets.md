---
date: 
title: Afterwork de la Serre à projets
picture: "/uploads/Afterwork.jpg"
publishdate: 2020-01-31T10:40:00+01:00

---
[L'appel à candidatures](https://www.laserre.org/appel-a-candidature/) de la Serre à projets est maintenant lancé. Il court jusqu'au 15 mars 2020.

Cet appel est très large et ouvert : les réponses collectives sont encouragées, et les variantes sont possibles : les candidats peuvent donc proposer des projets qui ne reprennent qu'une partie des études d'opportunité. Place à l'imagination donc !

Pour que l'équipe de la Serre à projets puisse répondre à toutes vos questions, et pour que les porteurs de projets puissent se rencontrer, rendez-vous donc à l'afterwork de la Serre à projets :

**Le jeudi 13 février à 17h30**

**Au Grand Sérieux,**

**27, rue Raugraff à Nancy**

Merci de vous inscrire par simple mail à contact@laserre.org.

A bientôt !