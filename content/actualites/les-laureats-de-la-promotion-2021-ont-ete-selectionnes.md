---
date: 
title: Les lauréats de la promotion 2021 ont été sélectionnés !
picture: "/uploads/onz74m0.jpg"
publishdate: 2021-04-14T09:00:00.000+02:00

---
## La Serre à projets a sélectionné ses **nouveaux lauréats** pour la promotion #2021 !

12 candidats ont été convoqués pour un entretien oral le 1er avril 2021 suite à l'appel à candidatures, **8 ont été retenus**.

Les projets sélectionnés sont les suivants :

* [**\[Consigne du verre\]** - L’**association Echogestes**](https://www.laserre.org/projets/2021/le-service-de-consigne-du-verre/ "https://www.laserre.org/projets/2021/le-service-de-consigne-du-verre/") qui propose un service de location, collecte et nettoyage d’écocups auquel s’ajouterait à terme un service de consigne du verre.
* [**\[Vélo-école\]**](https://www.laserre.org/projets/2021/la-velo-ecole/ "Vélo-école") - Un service de sensibilisation et de formation à la pratique du vélo et des EDPM porté par **Aurore Coince et Hadrien Fournet** qui sont soutenus par le collectif d’associations EDEN, Dynamo et VMA-GE
* [**\[Ville verte\]** - **Des Racines & des Liens**](https://www.laserre.org/projets/2021/la-ville-verte/ "Ville verte") qui souhaitent imaginer la ville de demain en la végétalisant de manière participative
* [**\[Bureau d'études Low-tech\]** -](https://www.laserre.org/projets/2021/bureau-d-etude-low-tech/ "BE Low tech") Le projet de **M. HAESSLER** : implantation d’une activité de bureau d’études low-tech pour accompagner les entreprises, associations, collectivités et particuliers sur la réalisation de leurs projets en proposant des alternatives sobres en lien avec la transition énergétique et environnementale.
* [**\[Cyclologistique\]** - **Les Coursiers Nancéiens**](https://www.laserre.org/projets/2021/le-service-de-livraison-a-velo/ "Cyclologistique") : création d'une coopérative de coursiers à vélo sur la Métropole nancéienne.
* **\[Volet blanc\]** :
  * [**Le jour d'après** :](https://www.laserre.org/projets/2021/la-cooperative-funeraire/ "Le jour d'après") projet de coopérative funéraire écoresponsable, éthique et solidaire
  * [**Lacagette** :](https://www.laserre.org/projets/2021/l-atelier-de-construction-mobile-et-participatif/ "Lacagette") projet d'atelier de construction mobile participatif  
    Remise : projet de réemploi de déchets du bâtiment
  * Une activité qui facilite le réemploi de déchets du bâtiment, portée par l’[**association Remise**.](https://www.laserre.org/projets/2021/la-valorisation-du-reemploi-dans-le-batiment/ "association Remise")

Ces 8 beaux projets vont bénéficier d'un accompagnement de l'équipe de la Serre et de ses partenaires, de l'étude au lancement de l'activité.

Vous en découvrirez davantage sur eux lors des prochains portraits de créateur !