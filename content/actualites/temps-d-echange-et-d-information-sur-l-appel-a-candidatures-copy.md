---
date: 
title: Temps d'échange sur l'appel à candidatures !
picture: "/uploads/temps-d-echange-et-d-information-2023.png"
publishdate: 2023-01-12T14:00:00+01:00

---
[L’appel à candidatures](https://www.laserre.org/appel-a-candidature/) de la Serre à projets est maintenant lancé ! Il court jusqu’au 12 février 2022.

Pour que l’équipe de la Serre à projets puisse répondre à toutes vos questions, et pour que les porteurs de projets puissent se rencontrer, rendez-vous donc au temps d'échange de la Serre à projets :

**Le jeudi 26 janvier à 17h30 au restaurant Un peu, Bocaux - 4 Rue des Ecuries 54500 Vandoeuvre-lès-Nancy**

Merci de vous y  inscrire en vous rendant sur le lien suivant : [https://framaforms.org/inscription-au-temps-dechange-de-la-serre-a-projets-1611571504](https://framaforms.org/inscription-au-temps-dechange-de-la-serre-a-projets-1611571504 "https://framaforms.org/inscription-au-temps-dechange-de-la-serre-a-projets-1611571504")

A bientôt !