---
date: 2019-12-11T20:44:41+01:00
title: Lancement officiel de la Serre à projets
picture: "/uploads/actu-lancement-officiel.png"
publishdate: 2020-01-07T20:44:41+01:00

---
Mercredi 27 novembre 2019 se tenait à la Cantoche, le restaurant associatif de la transition à Nancy, le lancement officiel de la Serre à projets. L’occasion d’un moment convivial pour présenter ce dispositif au service de la transition écologique du territoire et mettre en valeur l’engagement de [ses partenaires](../pages/partenaires.md).

Cette rencontre, organisée dans le cadre des rencontres territoriales proposées par le Département de Meurthe-et-Moselle, était le moment choisi pour présenter le dispositif à la presse, et préparer le terrain pour l’appel à candidatures qui sera lancé en janvier 2020. En attendant, les 7 idées sélectionnées par le comité de pilotage sont en train de faire l’objet d’études d’opportunité, afin de déterminer à quel point leur concrétisation serait utile au territoire, et sur quels leviers s’appuyer pour les développer. Leurs résultats seront présentés début janvier 2020