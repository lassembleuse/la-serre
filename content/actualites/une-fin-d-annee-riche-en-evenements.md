---
date: 
title: Une fin d'année riche en événements !
picture: "/uploads/inter-fabrique-avise-15112021.jpg"
publishdate: 2021-12-15T00:00:00+01:00

---
La fin d'année pour la Serre à projets a été riche en événements !

* Le 23 et 24 novembre la Serre à projets a participé aux **rencontres inter-Fabriques à Initiatives** à Paris, une opportunité pour partager autour de nos différents projets en cours des retours d’expérience et des bonnes pratiques métiers !

> **_Mais en fait, c’est quoi une FAI ?_**
>
> C'est un dispositif animé par l’[Avise](https://www.avise.org/ "Avise") et porté par une vingtaine de structures locales de l’accompagnement, la Fabrique à Initiatives accompagné les acteurs locaux, de la détection des besoins jusqu’à l’accompagnement du porteur de projet
>
> La Serre à projets s’inspire de cette méthodologie pour accompagner les futurs projets de la transition écologique du Sud 54

* Le 23 novembre avait également lieu la **Rencontre annuelle du réseau CollECtif Grand Est** à laquelle une partie de l'équipe de la Serre à projets était présente. Cette rencontre a permis de rencontrer des acteurs engagés dans des démarches liées à l'économie circulaire ! Plénière, ateliers découvertes et rencontres avec des porteurs de projets écologiquement ambitieux (dont [Frugali](https://www.frugali.fr/ "Frugali"), issu de la promo 2020 de la Serre) étaient au programme.
* Le 9 décembre avait lieu les **Rencontres régionales de l'économie circulaire** dans le cadre du programme [Climaxion](https://www.climaxion.fr/ "Climaxion"), porté par l'Ademe et la Région Grand Est. La Serre à projets est intervenue aux côtés de [La Benne Idée](https://labenneidee.fr/ "La Benne Idée") (recyclerie créative de mobilier issue de la promo 2020 de la Serre) pour parler réemploi. Nous avons présenté la toute jeune association et le déroulé de l'accompagnement. À noter également la présence de l'association Remise (issue de la promo 2021 de la Serre) qui était également invitée pour animer une intervention sur le réemploi dans le domaine du bâtiment.

Voilà de quoi faire le plein de contacts et de beaux projets en attendant le Père Noël et **_surtout_** [l'appel à candidatures 2022](https://www.laserre.org/appel-a-candidature/ "Appel à candidatures") qui ne saurait tardé à être annoncé !

**Un seul conseil : tenez-vous prêts !**