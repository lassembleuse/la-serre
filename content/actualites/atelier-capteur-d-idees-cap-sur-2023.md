---
date: 
title: 'Atelier capteur d''idées : cap sur 2023 !'
picture: "/uploads/img_20220613_160107.jpg"
publishdate: 2022-06-14T18:00:00+02:00

---
La Serre à projets a réuni ce lundi dernier un réseau de capteur d'idées afin d'avancer sur les prochaines thématiques qui seront présentées à [l'appel à candidatures](https://www.laserre.org/appel-a-candidature/ "https://www.laserre.org/appel-a-candidature/") pour l'année 2023 !

En amont de cet atelier, l'équipe de la Serre à projets a consulté plusieurs acteurs économiques pour connaître les besoins territoriaux en lien avec la transition écologique et imaginer des réponses collectives. Afin de donner corps aux idées retenues, un panel d'une vingtaine d'acteurs locaux engagés dans la transition écologique se sont réunis. Les participants avaient pour mission de débattre et de sélectionner les idées qui paraîtraient les plus intéressantes à développer au sein de notre Fabrique à projets.

Voici quelques idées qui ont été particulièrement plébiscitées :

* Garage Retrofit
* Valorisation des coproduits de brasserie et de boulangerie
* Atelier culinaire mobile
* Culture d'insectes et/ou de vers
* Outilthéque
* Recyclerie du sport
* Recyclerie du jouet
* Ecopaturage en ville
* Service d'optimisation des ressources en eau
* Lutte contre les espèces invasives
* Développement et renforcement des offres de couches lavables

Le comité de pilotage de la Serre à projets doit désormais prendre le relai afin de qualifier de manière définitive les idées à développer en 2023.