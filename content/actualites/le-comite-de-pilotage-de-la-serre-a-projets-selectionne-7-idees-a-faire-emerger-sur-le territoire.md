---
date: 2019-11-11T20:45:02+01:00
title: Le Comité de Pilotage de la Serre à projets sélectionne 7 idées
picture: "/uploads/actu-copil-sept_idees.jpg"
publishdate: 2019-12-11T20:45:02+01:00

---
Le Comité de Pilotage de la Serre à projets s’est réuni le jeudi 7 novembre 2019 à la Maison des Sports de Tomblaine afin d’opérer la sélection des idées d’activités qui vont faire l’objet d’une étude d’opportunité. [Les 25 idées](https://www.laserre.org/actualites/la-serre-a-projets-met-en-place-son-reseau-de-capteurs-d-idees/) issues du réseau de capteurs d’idées ont été soumises aux membres du comité. Le choix s’est opéré en fonction de 4 critères :

* L’impact économique, social et environnemental potentiel
* La contribution à la transition écologique territoriale
* Le caractère innovant
* La capacité des acteurs locaux à s’impliquer dans le projet

Après un vote et de nombreux échanges, chacun partageant son expertise et sa perception des besoins du territoire, ce sont 7 idées qui ont été sélectionnées :

* Le Tiers Lieu de la transition écologique
* La conserverie locale
* La cantine solidaire
* Le dispositif de sensibilisation à la cuisine responsable
* Le service de consigne du verre
* La recyclerie
* La trucothèque

L’équipe de la Serre va maintenant consacrer les deux mois à venir à étudier l’opportunité de chacune de ces idées, en se posant les questions suivantes : le projet correspond-t-il à un besoin ? A quelles conditions est-il opportun ? Quels sont les leviers possibles et les écueils à éviter ?

Une fois instruites, ces idées vont se transformer en projets, qui seront soumis à appel à candidature en janvier 2020. D’Ici là, n’hésitez à commencer à construire vos équipes et vos propositions !