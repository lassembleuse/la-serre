---
date: 
title: 'Derniers jours pour candidatez ! Vous hésitez ? Ecoutez nos interviews ! '
picture: "/uploads/time-concept-with-pen-keyboard-on-white-background-flat-lay-horizontal-image.jpg"
publishdate: 2022-02-14T11:00:00+01:00

---
Alors que l'**appel à candidatures touche bientôt à sa fin** ([vite, viiite, candidatez !](https://www.laserre.org/appel-a-candidature/ "Candidatez !")), nous faisons un point sur les interventions de La Serre à projets sur les différentes antennes radiophoniques de la Région !

En voici un récapitulatif :

* **France Bleu :** [https://lstu.fr/francebleu](https://lstu.fr/francebleu "https://lstu.fr/francebleu")
* **RCN (en vidéo !) :** [https://www.youtube.com/watch?v=S5YmNaq85cA](https://www.youtube.com/watch?v=S5YmNaq85cA "https://www.youtube.com/watch?v=S5YmNaq85cA")
* **Fajet :** [https://lstu.fr/fajet](https://lstu.fr/fajet "https://lstu.fr/fajet")
* **Radio Campus :** [https://www.radiocampuslorraine.com/2022/01/18/la-serre-a-projets/](https://www.radiocampuslorraine.com/2022/01/18/la-serre-a-projets/ "https://www.radiocampuslorraine.com/2022/01/18/la-serre-a-projets/")
* **RCF :** [https://rcf.fr/actualite/le-grand-invite-alsace?episode=188277](https://rcf.fr/actualite/le-grand-invite-alsace?episode=188277 "https://rcf.fr/actualite/le-grand-invite-alsace?episode=188277")

Bonne écoute !

[L’appel à candidatures court jusqu’au 16 février 2022.]() **Il n’est pas trop tard pour déposer un dossier !**

Des questions ? [**Contactez-nous !**](https://www.laserre.org/contact/ "Contactez-nous")