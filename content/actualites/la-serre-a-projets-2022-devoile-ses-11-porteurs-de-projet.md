---
date: 
title: La Serre à projets 2022 dévoile ses 11 porteurs de projet !
picture: "/uploads/visuel-selection-des-projets-2022-fb.png"
publishdate: 2022-03-01T11:03:00+01:00

---
### [Découvrez les **nouveaux lauréats** pour la promotion #2022 !](https://www.laserre.org/projets/2022/ "Promotion 2022")

Cette année ce ne sont pas moins de 17 candidats qui ont été convoqués pour un entretien oral, **11 ont été retenus**.

La **qualité** était au rendez-vous et il fût difficile de départager les différents porteurs de projet.

Et voici les projets qui ont été sélectionnés :

[**\[Ferme Florale\]**](https://www.laserre.org/projets/2022/la-production-et-vente-de-fleurs-locales/ "Ferme florale") une ferme florale cultivant des fleurs locales pour permettre un approvisionnement en circuit court des fleuristes du secteur - projet porté par **Caroline THINUS**

[**\[Vélo-bus et vélo-taxi\]**](https://www.laserre.org/projets/2022/le-velo-bus-et-ou-velo-taxi/ "Vélo-bus") une activité de vélo-bus et vélo-taxi à destination des habitants de la Métropole nancéienne – projet porté par **Eric JOFFRES et Maxime KAUFMANN**

[**\[Cantine Solidaire\]**](https://www.laserre.org/projets/2022/la-cantine-solidaire-quartiers-en-transition-vandoeuvre-copy/ "Cantine solidaire") une restauration favorisant le faire-ensemble, la convivialité et permettant l’accès à une alimentation saine et responsable à tous – un projet porté par [**LE** **VERT T’Y GO**](https://leverttygo.fr/ "le vert t'y go")

[**\[Biodéchets\]**](https://www.laserre.org/projets/2022/la-collecte-et-le-traitement-des-biodechets/ "Biodéchets") un projet de valorisation des biodéchets, avec notamment la mise en place d’une collecte et d’un tri desdits déchets – un projet porté par **l’**[**AEIM ADAPEI 54**](http://www.aeim54.fr/ "AEIM ADAPEI 54")

**\[Volet blanc\]** :

* [**La Recyclerie les 3R**](https://www.laserre.org/projets/2022/le-lavage-du-linge-ecoresponsable/ "La recyclerie les 3R") : une recyclerie textile afin de favoriser la seconde main et le réemploi par le biais de l’upcycling – projet porté par **Natacha GUIDET**
* [**Le café des enfants :**](https://www.laserre.org/projets/2022/le-cafe-des-enfants/ "Le café des enfants") un espace de partage, d'accueil et d’échange, où enfants et parents peuvent venir se ressourcer un peu “comme à la maison” – projet porté par **Magalie CURE et Charlotte RAVARY**
* [**Le réemploi du matériel numérique :**](https://www.laserre.org/projets/2022/la-valorisation-du-reemploi-dans-le-numerique/ "Le réemploi du matériel numérique") une activité visant à proposer du matériel reconditionné à des publics vulnérables et situation de précarité numérique – un projet co-porté par le groupement [**ULIS**](http://www.ulislorraine.fr/index.php/accueil "ULIS") **et** [**SOS FUTUR**](https://www.sos-futur.fr/ "SOS FUTUR")
* [**Le centre de médiation équine et une écurie active :**](https://www.laserre.org/projets/2022/le-centre-de-mediation-equine/ "Centre de médiation équine") une activité permettant la découverte de la nature et de la biodiversité à des personnes vulnérables – projet porté par **Emilie ADET**
* [**La Grappe :**](https://www.laserre.org/projets/2022/la-solution-d-auto-hebergement-libre-et-ecologique/ "La Grappe") un projet de cloud plus responsable visant à offrir une solution d’auto-hébergement alternative à l’existant – un projet porté par **Théo HUNERBLAES et Piet ROSE de** [**CODATTE**](https://codatte.fr/ "Codatte")

[**\[Quartiers en transition\]** :](https://www.laserre.org/quartier-en-transition/ "Quartiers en transition")

Depuis 2021, la Serre à projets déploie son dispositif au sein des quartiers prioritaires de la politique de la Ville (QPV) de la Métropole du Grand Nancy.

Les porteurs de projet retenus sont les suivants :

* [**KHAMSA :**](https://www.laserre.org/projets/2022/la-cantine-solidaire-quartiers-en-transition-et-serre-a-projets-generaliste/ "Khamsa") projet de cantine et traiteur solidaires – **Les Nations à Vandœuvre-lès-Nancy**
* [**SI L’ON SE PARLAIT :**](https://www.laserre.org/projets/2022/le-tiers-lieu-de-la-transition-quartiers-en-transition-laxou/ "Si l'on se parlait") projet de tiers lieu intégrant une recyclerie et une cantine solidaire – **Les Provinces à Laxou**

Les 11 porteurs de projets vont se réunir le 17 mars lors d'une journée d'intégration qui marquera le début de l'accompagnement.

Nous vous informerons des avancées de chacun. Restez connectés !