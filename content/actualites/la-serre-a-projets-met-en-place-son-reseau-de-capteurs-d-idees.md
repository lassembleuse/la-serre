---
date: 2019-11-11T19:44:41.000+00:00
title: La Serre à projets met en place son réseau de capteurs d’idées
picture: "/uploads/actu-reseau_capteur-idee.png"
publishdate: 2019-11-11T20:44:41+01:00

---
Faire remonter des besoins territoriaux en lien avec la transition écologique, voilà l’objectif des premiers mois d’activité de la Serre à projets. Pour ce faire, Kèpos et France Active Lorraine ont diffusé largement en juillet et août 2019 un questionnaire de remontée d’idées et de besoins. Tout un chacun (particuliers, entreprises, associations, collectivités) y était invité à faire feu de tout bois pour exprimer ses besoins et envies de nouvelles activités de transition. 70 réponses ont été obtenues ! Un bon résultat au cœur de l’été.

L’équipe de la Serre à projets s’est réunie début septembre 2019 pour dépouiller toutes ces contributions. Il en est ressorti une véritable effervescence d’idées et de projets, dans tous les domaines de la transition : habitat, mobilité, énergie, alimentation, économie circulaire… Parce qu’il fallait bien choisir, ce sont 25 idées qui ont été extraites de ce bouillonnement. Les voici !

* Alimentation :
  * Halles commerciales de la transition écologique
  * Dispositif de sensibilisation et formation à la cuisine responsable, bio et locale
  * Valorisation de cultures locales à fort intérêt nutritionnel et nécessitant peu d’entretien (ortie, chanvre, etc...)
  * Conserverie locale
* Économie circulaire :
  * Recyclerie
  * Maison du Zéro Déchets
  * Service de consigne du verre
* Habitat / Énergie :
  * Entreprise du bâtiment spécialisée dans l’écoconstruction
  * Ecolieu/Eco-hameau
  * Fabrication de solutions d’habitats légers, démontables et déplaçables
  * Dispositif d’investissement mutualisé dans des travaux de rénovation
* Environnement et biodiversité :
  * Paysagiste spécialisé en permaculture et végétalisation des toits et façades
  * Plate-forme « Végétalisons Nancy »
  * Système de fermage entre particuliers pour l’exploitation de potagers en friche, vergers…
* Économie et consommation :
  * Trucothèque/service de location des objets du quotidien
  * Tiers lieu de la transition écologique
  * Cabinet d’expertise comptable pratiquant la comptabilité en triple capital
* Éducation :
  * Dispositif de sensibilisation du jeune public
  * Application centralisant les bonnes pratiques en lien avec la transition écologique sur la région nancéienne
* Culture :
  * Troupe de spectacle vivant en lien avec la transition écologique
* Solidarités :
  * Cantine solidaire
  * Pompes funèbres écologiques

Pour aller plus loin, il était nécessaire de faire expertiser ces idées par des acteurs du territoire, en prise directe avec le terrain, afin de pouvoir cerner celles qui avaient le meilleur potentiel. C’est ce qui s’est passé à l’occasion d’un Workshop créatif qui s’est tenu le 27 septembre 2019 à Tomblaine, en présence d’une vingtaine d’acteurs locaux engagés dans la transition écologique. Des échanges passionnés ont permis de donner corps aux idées retenues. Peu à peu sont apparues des idées plus intéressantes que d’autres. Le panel d’acteurs était, en fin de session, invité à se prononcer sur ses idées favorites, via un vote indicatif. A charge alors pour le comité de pilotage de la Serre à projets, se réunissant quelques semaines plus tard, d’opérer la sélection définitive !: