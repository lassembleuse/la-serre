---
date: 
title: 'Revue de projets #8 : Remise'
picture: "/uploads/220429_remise_com_photos_groupesce_retouche.jpeg"
publishdate: 2022-05-23T10:00:00+02:00

---
En 2021, la Serre à projets a accompagné un projet de réemploi de matériaux de bâtiment baptisé [Remise](https://www.re-mise.fr/ "Remise").  De quoi s'agit-il ? Découvrez en plus !

**Qui êtes-vous ?**

Christelle Hopfner, Emilie Lemoine, Florent Collin, architectes DPLG + d’autres professionnels de la construction.

**Comment est né le projet de créer une association spécialisée dans le réemploi de la construction ?**

Nos professions nous emmènent régulièrement sur des chantiers.

A l’heure, de l’urgence climatique, nous ne pouvions plus supporter de voir d’un côté des quantités de déchets excessives sur les chantiers, et de l’autre de savoir ce que coûte la fabrication de matériaux (non-biosourcés) dans la construction.

Le réemploi nous est apparu comme une évidence ! Un acte militant.

Ces prises de consciences ont trouvé échos dans le mouvement de la Frugalité Heureuse et Créative Lorraine, pour lequel nous avons réaliser une carte locale des acteurs du réemploi.

Le constat du manque d’acteurs nous a poussé à créer l’association Remise, comme facilitateur du réemploi en Lorraine.

**En quoi consiste Remise ?**

L’association Remise est un support au développement du réemploi en Lorraine.

Elle porte la sensibilisation, l’émergence d’acteurs, la structuration d’une ou des filières de réemploi de constructions.

Remise permettra d’offrir des outils pour rendre possible le réemploi.

Nous souhaitons développer une plate-forme numérique inscrite dans le territoire lorrain.

Car le réemploi, c’est avant tout un territoire, et des échanges d’acteurs proches.

Remise permettra de référencer les acteurs qui œuvrent pour le réemploi (entreprises, architectes, bureau de contrôle, ….)

Enfin, l’association portera la mise en place d’une recycleriez physique. Qu’elle soit plate-forme de stockage temporaire, ou lieu de vente ouvert au public, un endroit physique est incontournable dans le processus du réemploi.

**Où en êtes-vous dans le projet ?**

_SENSIBILISATION :_

Nous avons déjà rencontrés beaucoup d’acteurs pour les sensibiliser au réemploi : collectivités, architectes frugaux, grands publics, et maîtres d’ouvrages publics.

Nous continuons à être sollicités pour parler du réemploi à des publics différents.

_SUBVENTIONS :_

Nous avons obtenu deux subventions du programme Climaxion, l’une pour nous aider à porter la plate-forme numérique, mais il nous manque des financements pour pouvoir la lancer.

L’autre pour la réalisation d’une étude de faisabilité de plate-forme physique du réemploi en Lorraine. L’étude débute, l’état de l’art est réalisé.

_STRUCTURATION :_

Nous cherchons également à consolider un modèle économique viable pour cette structure. Dans ce but, la serre à projets nous accompagne sur ce volet.

Pour des questions juridiques et assurentielles (proposés par un cabinet d’avocat spécialiste), en parallèle de l'association, nous créons un bureau ingénierie qui prend dans un premier temps la forme d'un collectif d’expertise en réemploi : **RE!NOUVEAU** - Architecture, conseils et réemploi.

Il permettra d’effectuer et de garantir des diagnostics réemploi et des missions de conseils.

_LOCAUX :_

L’association a trouvé également du soutien auprès de la communauté de Moselle-et-Madon qui leur a offert un terrain d’expérimentation avec le prêt d’un lieu de stockage de 500 m2 en bail précaire.

L’association OK3, qui gère l’exploitation du site de l’Octroi à Nancy, a accepté la candidature de Remise pour son installation dans un bureau de la Tour Nord.

**Qu’est-ce qui vous attend dans les prochains mois ?**

_SENSIBILISATION :_

D’autres évènements de sensibilisation sont prévus auprès du grand public, et des entreprises.

Nous continuons à relayer nos actions et des références architecturales en réemploi sur les réseaux sociaux également.

_SUBVENTIONS :_

Nous cherchons des financements complémentaires pour finaliser le projet de la plate-forme numérique.

L’association souhaite créer deux emplois dans le courant de l’année 2022.

Des recherches en subventions de fonctionnement sont prévues pour ce début d’année.

_STRUCTURATION :_

Nous souhaitons pérenniser par des emplois le développement de l’association.

_LOCAUX :_

L’expérimentation au sein de local de stockage continuera.

**Quel a été l’avantage de mener cette initiative dans le cadre de la Serre à projets ?**

La serre à projets nous a permis de nous sentir soutenu dans la création de projet de l’ESS sur le territoire nancéien.

Elle nous a apporté des compétences et des conseils pour la structuration de l'association.

L’échange avec d’autres structures, a permis de créer des synergies.

La plus-value de la serre à projets réside enfin dans l'engagement écologique que nous avons du prendre dans la création de notre projet.